#ifndef __ATL_DHTML_H__
#define __ATL_DHTML_H__

#ifndef __cplusplus
#error ATL requires C++ compilation (use a .cpp suffix)
#endif

#ifndef __mshtml_h__
#include <mshtml.h>
#endif

#ifndef __MSHTMDID_H__
#include <mshtmdid.h>
#endif

#ifndef __mshtmhst_h__
#include <mshtmhst.h>
#endif

#ifndef __ATLBASE_H__
#include <atlbase.h>
#endif

#ifndef __ATLCOM_H__
#include <atlcom.h>
#endif

#ifndef __exdisp_h__
#include <exdisp.h>
#endif

#ifndef EXDISPID_H_
#include <exdispid.h>
#endif

#ifndef __ATLWIN_H__
#include <atlwin.h>
//#error atldhtmldlg.h requires atlwin.h to be included first
#endif

#ifndef __ATLHOST_H__
#include <atlhost.h>
#endif

//#include <atlsafe.h>
#ifndef _OLEAUTO_H_
#include <Oleauto.h>
#endif // _OLEAUTO_H_

#ifdef __ATLDDX_H__
#ifndef WTL::DDX_LOAD
#error atldhtmldlg.h reqires atlddx.h(WTL) to be included first
#endif
using namespace WTL;
#endif

#define  _WTL_NO_CSTRING

namespace ATL {

// Classes declared in this file
class CDHtmlSinkHandler;  // Events Sink Base
class CDHtmlEventSink;  // IHTMLDocument2 Events Sink

// IDispatch
class CDHtmlControlSink;  // ActiveX Control Events Sink

// IDispatch
class CDHtmlElementEventSink; // IHTMLElement Events Sink

// IDispatch
class ExternalDispatchBase;
template<class T> class CExternalDispatchImpl; // External Dispatch Implement

// IDocHostUIHandleDispatch
template<class T> class IDocHostUIHandlerDispatchImpl;

// IDispatch
template<class T> class DWebBrowserEvent2Impl;

// ATL::CDialogImplBaseT
template<class T, class TBase> class DHtmlDialogImpl;
template<class T, class TBas> class MultiPageDHtmlDialogImpl;

enum DHtmlEventMapEntryType {
  DHTMLEVENTMAPENTRY_NAME,
  DHTMLEVENTMAPENTRY_CLASS,
  DHTMLEVENTMAPENTRY_TAG,
  DHTMLEVENTMAPENTRY_ELEMENT,
  DHTMLEVENTMAPENTRY_CONTROL,
  DHTMLEVENTMAPENTRY_END
};

struct DHtmlEventMapEntry;

// Dummy base classes just to force pointers-to-member that work with multiple inheritance
class DHtmlSinkHandlerBase1 {
};

class DHtmlSinkHandlerBase2 {
};

class CDHtmlSinkHandler :
  public DHtmlSinkHandlerBase1,
  public DHtmlSinkHandlerBase2 {
public:
  STDMETHOD(CDHtmlSinkHandlerQueryInterface)(REFIID riid, void** ppvObject) = 0;
  virtual ULONG STDMETHODCALLTYPE AddRef(void) = 0;
  virtual ULONG STDMETHODCALLTYPE Release(void) = 0;
  STDMETHOD(GetTypeInfoCount)(UINT* pctinfo) = 0;
  STDMETHOD(GetTypeInfo)(UINT iTInfo, LCID lcid, ITypeInfo** ppTInfo) = 0;
  STDMETHOD(GetIDsOfNames)(REFIID riid, OLECHAR** rgszNames, UINT cNames, LCID lcid, DISPID* rgDispId) = 0;
  STDMETHOD(Invoke)(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags,
                    DISPPARAMS* pDispParams, VARIANT* pVarResult, EXCEPINFO* pExcepInfo, UINT* puArgErr) = 0;
  virtual const DHtmlEventMapEntry* GetDHtmlEventMap() = 0;
};

// External Dispatch Method Map
struct DHtmlExternalMethodMapEntry {
  void (__stdcall ExternalDispatchBase::*pfnExternalFunc)();
  LPCTSTR szName; // external method name  
};

typedef void (ExternalDispatchBase::*EXTFUNC)(VARIANT* para1, VARIANT* para2, VARIANT* para3);
typedef void (__stdcall ExternalDispatchBase::*EXTFUNCCONTROL)();

#define BEGIN_EXTERNAL_METHOD_MAP(className) \
public: \
  virtual const DHtmlExternalMethodMapEntry* GetExternalMethodMapEntry(long* plNum) \
{ \
  typedef className theClass; \
  PTM_WARNING_DISABLE \
  static const DHtmlExternalMethodMapEntry _dhtmlExtMethodEntries[] = { \
{ _T("resizeWindow"), (EXTFUNCCONTROL)(EXTFUNC) &theClass::OnResizeWindow },\
{ _T("centerWindow"), (EXTFUNCCONTROL)(EXTFUNC) &theClass::OnCenterWindow },

#define END_EXTERNAL_METHOD_MAP() \
  /*{ NULL, (EXTFUNCCONTROL)NULL },*/ \
}; \
  PTM_WARNING_RESTORE \
  if (plNum) \
  *plNum = (long)(sizeof(_dhtmlExtMethodEntries)/sizeof(DHtmlExternalMethodMapEntry)); \
  return _dhtmlExtMethodEntries;\
}

#define EXTERNAL_METHOD(methodName, memberFxn) \
{ methodName, (EXTFUNCCONTROL)(EXTFUNC) &theClass::memberFxn },\
 
// DHtml Event Map
struct DHtmlEventMapEntry {
  DHtmlEventMapEntryType nType;
  DISPID dispId;
  LPCTSTR szName; // name or class based on nType
  void (__stdcall CDHtmlSinkHandler::*pfnEventFunc)();
};

typedef HRESULT(CDHtmlSinkHandler::*DHEVTFUNC)(IHTMLElement* pElement);
typedef void (__stdcall CDHtmlSinkHandler::*DHEVTFUNCCONTROL)();

#define BEGIN_DHTML_EVENT_MAP(className)\
public:\
  const DHtmlEventMapEntry* GetDHtmlEventMap()\
{\
  typedef className theClass;\
  PTM_WARNING_DISABLE \
  static const DHtmlEventMapEntry _dhtmlEventEntries[] = { \
{ DHTMLEVENTMAPENTRY_NAME, DISPID_EVMETH_ONREADYSTATECHANGE, NULL, (DHEVTFUNCCONTROL) (DHEVTFUNC) &theClass::OnDocumentReadyStateChange },

#define END_DHTML_EVENT_MAP()\
{ DHTMLEVENTMAPENTRY_END, 0, NULL, (DHEVTFUNCCONTROL) NULL },\
};\
  PTM_WARNING_RESTORE \
  return _dhtmlEventEntries;\
}

#define BEGIN_DHTML_EVENT_MAP_INLINE(className) BEGIN_DHTML_EVENT_MAP(className)
#define END_DHTML_EVENT_MAP_INLINE() END_DHTML_EVENT_MAP()


#define DHTML_EVENT(dispid, elemName, memberFxn)\
{ DHTMLEVENTMAPENTRY_NAME, dispid, elemName, (DHEVTFUNCCONTROL) (DHEVTFUNC) &theClass::memberFxn },\
 
#define DHTML_EVENT_CLASS(dispid, elemName, memberFxn)\
{ DHTMLEVENTMAPENTRY_CLASS, dispid, elemName, (DHEVTFUNCCONTROL) (DHEVTFUNC) &theClass::memberFxn },\
 
#define DHTML_EVENT_TAG(dispid, elemName, memberFxn)\
{ DHTMLEVENTMAPENTRY_TAG, dispid, elemName, (DHEVTFUNCCONTROL) (DHEVTFUNC) &theClass::memberFxn },\
 
#define DHTML_EVENT_ELEMENT(dispid, elemName, memberFxn)\
{ DHTMLEVENTMAPENTRY_ELEMENT, dispid, elemName, (DHEVTFUNCCONTROL) (DHEVTFUNC) &theClass::memberFxn },\
 
#define DHTML_EVENT_AXCONTROL(dispid, controlName, memberFxn)\
{ DHTMLEVENTMAPENTRY_CONTROL, dispid, controlName, (DHEVTFUNCCONTROL) (static_cast<void (__stdcall theClass::*)()>(&theClass::memberFxn)) },\
 
// specific commonly used events
#define DHTML_EVENT_ONHELP(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONHELP, elemName, memberFxn)\
 
#define DHTML_EVENT_ONCLICK(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONCLICK, elemName, memberFxn)\
 
#define DHTML_EVENT_ONDBLCLICK(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONDBLCLICK, elemName, memberFxn)\
 
#define DHTML_EVENT_ONKEYPRESS(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONKEYPRESS, elemName, memberFxn)\
 
#define DHTML_EVENT_ONKEYDOWN(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONKEYDOWN, elemName, memberFxn)\
 
#define DHTML_EVENT_ONKEYUP(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONKEYUP, elemName, memberFxn)\
 
#define DHTML_EVENT_ONMOUSEOUT(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONMOUSEOUT, elemName, memberFxn)\
 
#define DHTML_EVENT_ONMOUSEOVER(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONMOUSEOVER, elemName, memberFxn)\
 
#define DHTML_EVENT_ONMOUSEMOVE(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONMOUSEMOVE, elemName, memberFxn)\
 
#define DHTML_EVENT_ONMOUSEDOWN(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONMOUSEDOWN, elemName, memberFxn)\
 
#define DHTML_EVENT_ONMOUSEUP(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONMOUSEUP, elemName, memberFxn)\
 
#define DHTML_EVENT_ONSELECTSTART(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONSELECTSTART, elemName, memberFxn)\
 
#define DHTML_EVENT_ONFILTERCHANGE(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONFILTERCHANGE, elemName, memberFxn)\
 
#define DHTML_EVENT_ONDRAGSTART(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONDRAGSTART, elemName, memberFxn)\
 
#define DHTML_EVENT_ONBEFOREUPDATE(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONBEFOREUPDATE, elemName, memberFxn)\
 
#define DHTML_EVENT_ONAFTERUPDATE(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONAFTERUPDATE, elemName, memberFxn)\
 
#define DHTML_EVENT_ONERRORUPDATE(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONERRORUPDATE, elemName, memberFxn)\
 
#define DHTML_EVENT_ONROWEXIT(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONROWEXIT, elemName, memberFxn)\
 
#define DHTML_EVENT_ONROWENTER(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONROWENTER, elemName, memberFxn)\
 
#define DHTML_EVENT_ONDATASETCHANGED(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONDATASETCHANGED, elemName, memberFxn)\
 
#define DHTML_EVENT_ONDATAAVAILABLE(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONDATAAVAILABLE, elemName, memberFxn)\
 
#define DHTML_EVENT_ONDATASETCOMPLETE(elemName, memberFxn)\
  DHTML_EVENT(DISPID_HTMLELEMENTEVENTS_ONDATASETCOMPLETE, elemName, memberFxn)\
 
// non-bubbled element events
#define DHTML_EVENT_ONBLUR(elemName, memberFxn)\
  DHTML_EVENT_ELEMENT(DISPID_EVMETH_ONBLUR, elemName, memberFxn)\
 
#define DHTML_EVENT_ONCHANGE(elemName, memberFxn)\
  DHTML_EVENT_ELEMENT(DISPID_EVMETH_ONCHANGE, elemName, memberFxn)\
 
#define DHTML_EVENT_ONFOCUS(elemName, memberFxn)\
  DHTML_EVENT_ELEMENT(DISPID_EVMETH_ONFOCUS, elemName, memberFxn)\
 
#define DHTML_EVENT_ONRESIZE(elemName, memberFxn)\
  DHTML_EVENT_ELEMENT(DISPID_EVMETH_ONRESIZE, elemName, memberFxn)\
 
class CDHtmlEventSink : public CDHtmlSinkHandler {
public:
  HRESULT ConnectToConnectionPoint(IUnknown* punkObj, REFIID riid, DWORD* pdwCookie) {
    return AtlAdvise(punkObj, (IDispatch*)(this), riid, pdwCookie);
  }

  void DisconnectFromConnectionPoint(IUnknown* punkObj, REFIID riid, DWORD& dwCookie) {
    AtlUnadvise(punkObj, riid, dwCookie);
  }

  STDMETHOD(CDHtmlSinkHandlerQueryInterface)(REFIID iid, LPVOID* ppvObj) {
    if(!ppvObj)
      return E_POINTER;

    *ppvObj = NULL;

    if(IsEqualIID(iid, __uuidof(IDispatch)) || IsEqualIID(iid, __uuidof(IUnknown))) {
      *ppvObj = (IDispatch*)(this);
      AddRef();
      return S_OK;
    }

    return E_NOINTERFACE;
  }

  STDMETHOD_(ULONG, AddRef)() { return 1; }
  STDMETHOD_(ULONG, Release)() { return 1; }

  STDMETHOD(GetTypeInfoCount)(UINT* pctinfo) {
    *pctinfo = 0;
    ATLTRACENOTIMPL(_T("CDHtmlEventSink::GetTypeInfoCount"));
  }

  STDMETHOD(GetTypeInfo)(UINT /*iTInfo*/, LCID /*lcid*/, ITypeInfo** ppTInfo) {
    *ppTInfo = NULL;
    ATLTRACENOTIMPL(_T("CDHtmlEventSink::GetTypeInfo"));
  }

  STDMETHOD(GetIDsOfNames)(REFIID /*riid*/, OLECHAR** /*rgszNames*/, UINT /*cNames*/, LCID /*lcid*/, DISPID* /*rgDispId*/) {
    ATLTRACENOTIMPL(_T("CDHtmlEventSink::GetIDsOfNames"));
  }

  STDMETHOD(Invoke)(DISPID dispIdMember, REFIID /*riid*/, LCID /*lcid*/, WORD /*wFlags*/, DISPPARAMS* pDispParams, VARIANT* pVarResult, EXCEPINFO* pExcepInfo, UINT* puArgErr) {
    IHTMLElement* psrcElement = NULL;
    HRESULT hr = S_OK;

    VariantInit(pVarResult);

    if(DHtmlEventHook(&hr, dispIdMember, pDispParams, pVarResult, pExcepInfo, puArgErr))
      return hr;

    const DHtmlEventMapEntry* pMap = GetDHtmlEventMap();

    int nIndex = FindDHtmlEventEntry(pMap, dispIdMember, &psrcElement);

    if(nIndex < 0)
      return DISP_E_MEMBERNOTFOUND;

    // now call it
    if(pMap) {
      hr = (this->*((DHEVTFUNC)(GetDHtmlEventMap()[nIndex].pfnEventFunc)))(psrcElement);

      if(GetDHtmlEventMap()[nIndex].nType != DHTMLEVENTMAPENTRY_CONTROL && pVarResult) {
        pVarResult->vt = VT_BOOL;
        pVarResult->boolVal = (hr == S_OK) ? ATL_VARIANT_TRUE : ATL_VARIANT_FALSE;
      }
    }

    if(psrcElement)
      psrcElement->Release();

    return hr;
  }

  virtual BOOL DHtmlEventHook(HRESULT* /*phr*/, DISPID /*dispIdMember*/, DISPPARAMS* /*pDispParams*/, VARIANT* /*pVarResult*/, EXCEPINFO* /*pExcepInfo*/, UINT* /*puArgErr*/) {
    // stub base implementation
    return FALSE;
  }

  virtual const DHtmlEventMapEntry* GetDHtmlEventMap() = 0;
  virtual HRESULT GetDHtmlDocument(IHTMLDocument2** pphtmlDoc) = 0;
  //virtual void _OnBeforeNavigate2(LPDISPATCH pDisp, VARIANT FAR* URL, VARIANT FAR* Flags, VARIANT FAR* TargetFrameName, VARIANT FAR* PostData, VARIANT FAR* Headers, BOOL FAR* Cancel) {}
  //virtual void _OnNavigateComplete2(LPDISPATCH pDisp, VARIANT FAR* URL) {}
  //virtual void _OnDocumentComplete(LPDISPATCH pDisp, VARIANT* URL) {}

  int FindDHtmlEventEntry(const DHtmlEventMapEntry* pEventMap, DISPID dispIdMember, IHTMLElement** ppsrcElement) {
    HRESULT hr = DISP_E_MEMBERNOTFOUND;
    CComPtr<IHTMLWindow2> sphtmlWnd;
    CComPtr<IHTMLEventObj> sphtmlEvent;
    CComPtr<IHTMLElement> sphtmlElement;
    CComPtr<IHTMLDocument2> sphtmlDoc;
    CComBSTR bstrId, bstrName;
    CComBSTR bstrClass;
    CComBSTR bstrTagName;

    int i;
    int nIndexFound = -1;

    if(ppsrcElement == NULL)
      return E_POINTER;

    *ppsrcElement = NULL;

    if(!pEventMap)
      goto Error;

    // get the html document
    hr = GetDHtmlDocument(&sphtmlDoc);

    if(sphtmlDoc == NULL)
      goto Error;

    // get the element that generated the event
    sphtmlDoc->get_parentWindow(&sphtmlWnd);

    if((sphtmlWnd == NULL) || FAILED(sphtmlWnd->get_event(&sphtmlEvent)) || (sphtmlEvent == NULL)) {
      hr = DISP_E_MEMBERNOTFOUND;
      goto Error;
    }

    sphtmlEvent->get_srcElement(&sphtmlElement);
    *ppsrcElement = sphtmlElement;

    if(sphtmlElement)
      sphtmlElement.p->AddRef();

    // look for the dispid in the map
    for(i = 0; pEventMap[i].nType != DHTMLEVENTMAPENTRY_END; i++) {
      if(pEventMap[i].dispId == dispIdMember) {
        if(pEventMap[i].nType == DHTMLEVENTMAPENTRY_NAME) {
          if(!bstrId && sphtmlElement)
            sphtmlElement->get_id(&bstrId);

          if(bstrId && pEventMap[i].szName && !wcscmp(bstrId, CComBSTR(pEventMap[i].szName)) ||
              (!bstrId && !sphtmlElement)) {
            nIndexFound = i;
            break;
          }

        } else if(pEventMap[i].nType == DHTMLEVENTMAPENTRY_CLASS) {
          if(!bstrClass && sphtmlElement)
            sphtmlElement->get_className(&bstrClass);

          if(bstrClass && !wcscmp(bstrClass, CComBSTR(pEventMap[i].szName))) {
            nIndexFound = i;
            break;
          }

        } else if(pEventMap[i].nType == DHTMLEVENTMAPENTRY_TAG) {
          if(!bstrTagName && sphtmlElement)
            sphtmlElement->get_tagName(&bstrTagName);

          if(bstrTagName && !_wcsicmp(bstrTagName, CComBSTR(pEventMap[i].szName))) {
            nIndexFound = i;
            break;
          }
        }
      }
    }

Error:

    if(nIndexFound == -1 && *ppsrcElement) {
      (*ppsrcElement)->Release();
      *ppsrcElement = NULL;
    }

    return nIndexFound;
  }
};

class CDHtmlControlSink : public IDispatch {
public:
  LPCTSTR m_szControlId;
  DWORD m_dwCookie;
  ATL::CComPtr<IUnknown> m_spunkObj;
  IID m_iid;
  GUID m_libid;
  WORD m_wMajor;
  WORD m_wMinor;
  ATL::CComPtr<ITypeInfo> m_spTypeInfo;
  CDHtmlSinkHandler* m_pHandler;
  DWORD_PTR m_dwThunkOffset;

  CDHtmlControlSink() {
    m_dwCookie = 0;
    m_pHandler = NULL;
    m_dwThunkOffset = 0;
    memset(&m_iid, 0x00, sizeof(IID));
  }

  CDHtmlControlSink(IUnknown* punkObj, CDHtmlSinkHandler* pHandler, LPCTSTR szControlId, DWORD_PTR dwThunkOffset = 0) {
    m_dwCookie = 0;
    m_pHandler = pHandler;
    m_szControlId = szControlId;
    m_dwThunkOffset = dwThunkOffset;
    ConnectToControl(punkObj);
  }

  ~CDHtmlControlSink() {
    if(m_dwCookie != 0)
      AtlUnadvise(m_spunkObj, m_iid, m_dwCookie);
  }

  HRESULT ConnectToControl(IUnknown* punkObj) {
    m_spunkObj = punkObj;
    HRESULT hr = AtlGetObjectSourceInterface(punkObj, &m_libid, &m_iid, &m_wMajor, &m_wMinor);

    if(FAILED(hr))
      return hr;

    CComPtr<ITypeLib> spTypeLib;

    hr = LoadRegTypeLib(m_libid, m_wMajor, m_wMinor, LOCALE_USER_DEFAULT, &spTypeLib);

    if(FAILED(hr))
      return hr;

    hr = spTypeLib->GetTypeInfoOfGuid(m_iid, &m_spTypeInfo);

    if(FAILED(hr))
      return hr;

    return AtlAdvise(punkObj, this, m_iid, &m_dwCookie);
  }

  STDMETHOD_(ULONG, AddRef)() { return 1; }
  STDMETHOD_(ULONG, Release)() { return 1; }

  STDMETHOD(QueryInterface)(REFIID iid, LPVOID* ppvObj) {
    if(!ppvObj)
      return E_POINTER;

    *ppvObj = NULL;

    if(IsEqualIID(iid, __uuidof(IUnknown)) ||
        IsEqualIID(iid, __uuidof(IDispatch)) ||
        IsEqualIID(iid, m_iid)) {
      *ppvObj = this;
      return S_OK;
    }

    return E_NOINTERFACE;
  }

  STDMETHOD(GetTypeInfoCount)(UINT* pctinfo) {
    *pctinfo = 0;
    ATLTRACENOTIMPL(_T("CDHtmlControlSink::GetTypeInfoCount"));
  }

  STDMETHOD(GetTypeInfo)(UINT /*iTInfo*/, LCID /*lcid*/, ITypeInfo** ppTInfo) {
    *ppTInfo = NULL;
    ATLTRACENOTIMPL(_T("CDHtmlControlSink::GetTypeInfo"));
  }

  STDMETHOD(GetIDsOfNames)(REFIID /*riid*/, OLECHAR** /*rgszNames*/, UINT /*cNames*/, LCID /*lcid*/, DISPID* /*rgDispId*/) {
    ATLTRACENOTIMPL(_T("CDHtmlControlSink::GetIDsOfNames"));
  }

  STDMETHOD(Invoke)(DISPID dispidMember, REFIID /*riid*/, LCID lcid, WORD /*wFlags*/, DISPPARAMS* pdispparams, VARIANT* pvarResult, EXCEPINFO* /*pExcepInfo*/, UINT* /*puArgErr*/) {
    ATLASSERT(m_pHandler);

    if(m_pHandler == NULL)
      return S_OK;

    _ATL_FUNC_INFO info;

    BOOL fFound = FALSE;
    DHEVTFUNCCONTROL pEvent = NULL;

    const DHtmlEventMapEntry* pEventMap = m_pHandler->GetDHtmlEventMap();

    for(int i = 0; pEventMap[i].nType != DHTMLEVENTMAPENTRY_END; i++) {
      if(pEventMap[i].nType == DHTMLEVENTMAPENTRY_CONTROL &&
          pEventMap[i].dispId == dispidMember &&
          !_tcscmp(pEventMap[i].szName, m_szControlId)) {
        // found the entry
        pEvent = pEventMap[i].pfnEventFunc;
        fFound = TRUE;
        break;
      }
    }

    if(!fFound)
      return DISP_E_MEMBERNOTFOUND;

    HRESULT hr = GetFuncInfoFromId(m_iid, dispidMember, lcid, info);

    if(FAILED(hr)) {
      return S_OK;
    }

    return InvokeFromFuncInfo(pEvent, info, pdispparams, pvarResult);
  }

  //Helper for invoking the event
  HRESULT InvokeFromFuncInfo(DHEVTFUNCCONTROL pEvent, ATL::_ATL_FUNC_INFO& info, DISPPARAMS* pdispparams, VARIANT* pvarResult) {
    USES_ATL_SAFE_ALLOCA;

    if(info.nParams < 0) {
      return E_INVALIDARG;
    }

    if(info.nParams > size_t(-1) / sizeof(VARIANTARG*)) {
      return E_OUTOFMEMORY;
    }

    VARIANTARG** pVarArgs = info.nParams ? (VARIANTARG**)_ATL_SAFE_ALLOCA(sizeof(VARIANTARG*)*info.nParams, _ATL_SAFE_ALLOCA_DEF_THRESHOLD) : 0;

    if(!pVarArgs) {
      return E_OUTOFMEMORY;
    }

    for(int i = 0; i < info.nParams; i++) {
      pVarArgs[i] = &pdispparams->rgvarg[info.nParams - i - 1];
    }

    CComStdCallThunk<CDHtmlSinkHandler> thunk;

    if(m_pHandler)
      thunk.Init(pEvent, reinterpret_cast< CDHtmlSinkHandler* >((DWORD_PTR) m_pHandler - m_dwThunkOffset));

    CComVariant tmpResult;

    if(pvarResult == NULL)
      pvarResult = &tmpResult;

    HRESULT hr = DispCallFunc(
                   &thunk,
                   0,
                   info.cc,
                   info.vtReturn,
                   info.nParams,
                   info.pVarTypes,
                   pVarArgs,
                   pvarResult);
    ATLASSERT(SUCCEEDED(hr));
    return hr;
  }

  HRESULT GetFuncInfoFromId(const IID& iid, DISPID dispidMember, LCID lcid, ATL::_ATL_FUNC_INFO& info) {
    if(!m_spTypeInfo)
      return E_FAIL;

    return AtlGetFuncInfoFromId(m_spTypeInfo, iid, dispidMember, lcid, info);
  }

  VARTYPE GetUserDefinedType(ITypeInfo* pTI, HREFTYPE hrt) {
    return AtlGetUserDefinedType(pTI, hrt);
  }
};

class CDHtmlElementEventSink : public IDispatch {
public:
  CDHtmlEventSink* m_pHandler;
  ATL::CComPtr<IUnknown> m_spunkElem;
  DWORD m_dwCookie;

  CDHtmlElementEventSink(CDHtmlEventSink* pHandler, IDispatch* pdisp) {
    m_pHandler = pHandler;
    pdisp->QueryInterface(__uuidof(IUnknown), (void**) &m_spunkElem);
    m_dwCookie = 0;
  }

  STDMETHOD_(ULONG, AddRef)() { return 1; }
  STDMETHOD_(ULONG, Release)() { return 1; }

  STDMETHOD(QueryInterface)(REFIID iid, LPVOID* ppvObj) {
    if(!ppvObj)
      return E_POINTER;

    *ppvObj = NULL;

    if(IsEqualIID(iid, __uuidof(IUnknown)) ||
        IsEqualIID(iid, __uuidof(IDispatch))) {
      *ppvObj = this;
      return S_OK;
    }

    return E_NOINTERFACE;
  }

  STDMETHOD(GetTypeInfoCount)(UINT* pctinfo) {
    *pctinfo = 0;
    ATLTRACENOTIMPL(_T("CDHtmlElementEventSink::GetTypeInfoCount"));
  }

  STDMETHOD(GetTypeInfo)(UINT /*iTInfo*/, LCID /*lcid*/, ITypeInfo** ppTInfo) {
    *ppTInfo = NULL;
    ATLTRACENOTIMPL(_T("CDHtmlElementEventSink::GetTypeInfo"));
  }

  STDMETHOD(GetIDsOfNames)(REFIID /*riid*/, OLECHAR** /*rgszNames*/, UINT /*cNames*/, LCID /*lcid*/, DISPID* /*rgDispId*/) {
    ATLTRACENOTIMPL(_T("CDHtmlElementEventSink::GetIDsOfNames"));
  }

  STDMETHOD(Invoke)(DISPID dispIdMember, REFIID /*riid*/, LCID /*lcid*/, WORD /*wFlags*/, DISPPARAMS* /*pdispparams*/, VARIANT* pVarResult, EXCEPINFO* /*pExcepInfo*/, UINT* /*puArgErr*/) {
    HRESULT hr;
    CComPtr<IHTMLWindow2> sphtmlWnd;
    CComPtr<IHTMLDocument2> sphtmlDoc;
    CComPtr<IHTMLElement> sphtmlElem;
    CComPtr<IHTMLElement> spsrcElem;
    CComPtr<IHTMLEventObj> sphtmlEvent;

    CComBSTR bstrId;

    if(pVarResult)
      VariantInit(pVarResult);

    hr = m_spunkElem->QueryInterface(&sphtmlElem);

    if(!sphtmlElem)
      return hr;

    hr = sphtmlElem->get_id(&bstrId);

    if(FAILED(hr))
      return hr;

    hr = m_pHandler->GetDHtmlDocument(&sphtmlDoc);

    if(FAILED(hr))
      return hr;

    hr = sphtmlDoc->get_parentWindow(&sphtmlWnd);

    if(FAILED(hr))
      return hr;

    hr = sphtmlWnd->get_event(&sphtmlEvent);

    if(FAILED(hr))
      return hr;

    hr = sphtmlEvent->get_srcElement(&spsrcElem);

    if(FAILED(hr))
      return hr;

    const DHtmlEventMapEntry* pEventMap = m_pHandler->GetDHtmlEventMap();

    for(int i = 0; pEventMap[i].nType != DHTMLEVENTMAPENTRY_END; i++) {
      if(pEventMap[i].nType != DHTMLEVENTMAPENTRY_CONTROL &&
          pEventMap[i].dispId == dispIdMember) {
        if(pEventMap[i].szName && !wcscmp(CComBSTR(pEventMap[i].szName), bstrId)) {
          // found the entry
          hr = (m_pHandler->*((DHEVTFUNC)(m_pHandler->GetDHtmlEventMap()[i].pfnEventFunc)))(spsrcElem);

          if(pVarResult) {
            pVarResult->vt = VT_BOOL;
            pVarResult->boolVal = (hr == S_OK) ? ATL_VARIANT_TRUE : ATL_VARIANT_FALSE;
          }

          return S_OK;
        }
      }
    }

    return DISP_E_MEMBERNOTFOUND;
  }

  HRESULT Advise(LPUNKNOWN pUnkObj, REFIID iid) {
    return AtlAdvise((LPUNKNOWN)pUnkObj, (LPDISPATCH)this, iid, &m_dwCookie);
  }

  HRESULT UnAdvise(LPUNKNOWN pUnkObj, REFIID /*iid*/) {
    return AtlUnadvise((LPUNKNOWN)pUnkObj, __uuidof(HTMLElementEvents), m_dwCookie);
  }
};

class CExternalDispatchBase {};

template<class T>
class CExternalDispatchImpl : public IDispatch, public CExternalDispatchBase {
public:
  STDMETHOD_(ULONG, AddRef)() { return 1; }
  STDMETHOD_(ULONG, Release)() { return 1; }
  STDMETHOD(QueryInterface)(REFIID iid, LPVOID* ppvObj) {
    if(!ppvObj)
      return E_POINTER;

    *ppvObj = NULL;

    if(IsEqualIID(iid, __uuidof(IUnknown)) ||
        IsEqualIID(iid, __uuidof(IDispatch))) {
      *ppvObj = this;
      return S_OK;
    }

    return E_NOINTERFACE;
  }
  STDMETHOD(GetTypeInfoCount)(UINT* pctinfo) {
    *pctinfo = 0;
    ATLTRACENOTIMPL(_T("CExternalDispatchImpl::GetTypeInfoCount"));
  }
  STDMETHOD(GetTypeInfo)(UINT /*iTInfo*/, LCID /*lcid*/, ITypeInfo** ppTInfo) {
    *ppTInfo = NULL;
    ATLTRACENOTIMPL(_T("CExternalDispatchImpl::GetTypeInfo"));
  }
  STDMETHOD(GetIDsOfNames)(REFIID /*riid*/, OLECHAR** rgszNames, UINT cNames, LCID /*lcid*/, DISPID* rgDispId) {
    for(UINT i = 0; i < cNames; i++) {
      rgDispId[i] = DISPID_UNKNOWN;
    }

    ATLASSERT(rgszNames);

    if(cNames != 1)
      return DISP_E_UNKNOWNNAME; // named parameter has not been supported!!!

    long lMapNum = 0;
    const DHtmlExternalMethodMapEntry* pMap = (static_cast<T*>(this))->_GetExternalMethodMapEntry(&lMapNum);
    ATLASSERT(pMap);

    CComBSTR strName = rgszNames[0];

    for(long i = 0; i < lMapNum; i++) {
      if(pMap[i].szName == NULL)
        continue;

      CComBSTR strMapName = pMap[i].szName;

      if(strMapName == strName) {
        *rgDispId = i;
        return S_OK;
      }
    }

    return DISP_E_UNKNOWNNAME;
  }
  STDMETHOD(Invoke)(DISPID dispIdMember, REFIID /*riid*/, LCID /*lcid*/, WORD /*wFlags*/, DISPPARAMS* pdispparams, VARIANT* pVarResult, EXCEPINFO* /*pExcepInfo*/, UINT* /*puArgErr*/) {
    //HRESULT hr;
    if(pVarResult)
      VariantClear(pVarResult);

    return (static_cast<T*>(this))->OnExternalInvoke(dispIdMember, pdispparams);
  }

  IDispatch* GetExternalDispatch() {
    return (IDispatch*)this;
  }
};

template<class T>
class IDocHostUIHandlerDispatchImpl : public IDocHostUIHandlerDispatch {
public:
  // IUnknown methods
  STDMETHOD_(ULONG, AddRef)() { return 1; }
  STDMETHOD_(ULONG, Release)() { return 1; }
  STDMETHOD(QueryInterface)(REFIID iid, LPVOID* ppvObj) {
    if(!ppvObj)
      return E_POINTER;

    *ppvObj = NULL;

    if(IsEqualIID(iid, __uuidof(IUnknown)) ||
        IsEqualIID(iid, __uuidof(IDispatch)) ||
        IsEqualIID(iid, __uuidof(IDocHostUIHandlerDispatch))) {
      *ppvObj = this;
      return S_OK;
    }

    return E_NOINTERFACE;
  }

  // IDispatch methods
  STDMETHOD(GetTypeInfoCount)(UINT* /*pctinfo*/) {
    ATLTRACENOTIMPL(_T("IDocHostUIHandlerDispatchImpl::GetTypeInfoCount"));
  }
  STDMETHOD(GetTypeInfo)(UINT /*iTInfo*/, LCID /*lcid*/, ITypeInfo** ppTInfo) {
    *ppTInfo = NULL;
    ATLTRACENOTIMPL(_T("IDocHostUIHandlerDispatchImpl::GetTypeInfo"));
  }
  STDMETHOD(GetIDsOfNames)(REFIID /*riid*/, OLECHAR** /*rgszNames*/, UINT /*cNames*/, LCID /*lcid*/, DISPID* /*rgDispId*/) {
    ATLTRACENOTIMPL(_T("IDocHostUIHandlerDispatchImpl::GetIDsOfNames"));
  }
  STDMETHOD(Invoke)(DISPID /*dispIdMember*/, REFIID /*riid*/, LCID /*lcid*/, WORD /*wFlags*/,
                    DISPPARAMS* /*pdispparams*/, VARIANT* /*pVarResult*/, EXCEPINFO* /*pExcepInfo*/, UINT* /*puArgErr*/) {
    ATLTRACENOTIMPL(_T("IDocHostUIHandlerDispatchImpl::Invoke"));
  }

  // IDocHostUIHandlerDispatch methods
  STDMETHOD(ShowContextMenu)(DWORD /*dwID*/, DWORD /*x*/, DWORD /*y*/, IUnknown* /*pcmdtReserved*/, IDispatch* /*pdispReserved*/, HRESULT* dwRetVal) {
    *dwRetVal = S_OK;
    return S_OK;
  }
  STDMETHOD(GetHostInfo)(DWORD* pdwFlags, DWORD* /*pdwDoubleClick*/) {
    *pdwFlags = (static_cast<T*>(this))->m_dwHostFlags;
    return S_OK;
  }
  STDMETHOD(ShowUI)(DWORD /*dwID*/, IUnknown* /*pActiveObject*/, IUnknown* /*pCommandTarget*/, IUnknown* /*pFrame*/, IUnknown* /*pDoc*/, HRESULT* dwRetVal) {
    *dwRetVal = S_FALSE;
    return S_OK;
  }
  STDMETHOD(HideUI)(void) {
    return E_NOTIMPL;
  }
  STDMETHOD(UpdateUI)(void) {
    return E_NOTIMPL;
  }
  STDMETHOD(EnableModeless)(VARIANT_BOOL /*fEnable*/) {
    return E_NOTIMPL;
  }
  STDMETHOD(OnDocWindowActivate)(VARIANT_BOOL /*fActivate*/) {
    return E_NOTIMPL;
  }
  STDMETHOD(OnFrameWindowActivate)(VARIANT_BOOL /*fActivate*/) {
    return E_NOTIMPL;
  }
  STDMETHOD(ResizeBorder)(long /*left*/, long /*top*/, long /*right*/, long /*bottom*/, IUnknown* /*pUIWindow*/, VARIANT_BOOL /*fFrameWindow*/) {
    return E_NOTIMPL;
  }
#define KEY_DOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)
#define CTRL_DOWN() KEY_DOWN(VK_CONTROL)
#define ALT_DOWN() KEY_DOWN(VK_MENU)
#define SHIFT_DOWN() KEY_DOWN(VK_SHIFT)
  STDMETHOD(TranslateAccelerator)(DWORD_PTR /*hWnd*/, DWORD nMessage, DWORD_PTR wParam, DWORD_PTR /*lParam*/, BSTR /*bstrGuidCmdGroup*/, DWORD /*nCmdID*/, HRESULT* dwRetVal) {
    if((nMessage == WM_KEYDOWN || nMessage == WM_KEYUP)) {
#ifdef _DEBUG

      if(wParam == VK_F5) {
        *dwRetVal = S_FALSE;
        return S_OK;
      }

#endif // _DEBUG

      if(wParam == VK_BACK ||
          (wParam >= VK_F1 && wParam <= VK_F12) ||
          wParam == VK_ESCAPE ||
          (ALT_DOWN() && (wParam == VK_HOME || wParam == VK_LEFT || wParam == VK_RIGHT)) ||
          (CTRL_DOWN())) {
        *dwRetVal = S_OK;
        return S_OK;
      }

    } else if((nMessage == WM_SYSKEYDOWN || nMessage == WM_SYSKEYUP) && wParam != VK_MENU) {
      if(wParam == VK_HOME || wParam == VK_LEFT || wParam == VK_RIGHT) {
        *dwRetVal = S_OK;
        return S_OK;
      }
    }

    *dwRetVal = S_FALSE;
    return S_OK;
  }
  STDMETHOD(GetOptionKeyPath)(BSTR* pbstrKey, DWORD /*dw*/) {
    *pbstrKey = NULL;
    return E_NOTIMPL;
  }
  STDMETHOD(GetDropTarget)(IUnknown* /*pDropTarget*/, IUnknown** ppDropTarget) {
    *ppDropTarget = NULL;
    return E_NOTIMPL;
  }
  STDMETHOD(GetExternal)(IDispatch** ppDispatch) {
    if(ppDispatch == NULL)
      return E_POINTER;

    *ppDispatch = NULL;
    T* pThis = static_cast<T*>(this);

    if(pThis->CanAccessExternal()) {
      *ppDispatch = pThis->GetExternalDispatch();
      return S_OK;
    }

    return E_NOTIMPL;
  }
  STDMETHOD(TranslateUrl)(DWORD /*dwTranslate*/, BSTR /*bstrURLIn*/, BSTR* pbstrURLOut) {
    if(pbstrURLOut == NULL)
      return E_POINTER;

    *pbstrURLOut = NULL;
    return S_FALSE;
  }
  STDMETHOD(FilterDataObject)(IUnknown* /*pDO*/, IUnknown** ppDORet) {
    if(ppDORet == NULL)
      return E_POINTER;

    *ppDORet = NULL;
    return S_FALSE;
  }
};

template<class T>
class DWebBrowserEvent2Impl : public IDispatch {
public:
  DWebBrowserEvent2Impl() : m_dwCookie(0) {}

public:
  // IUnknown methods
  STDMETHOD_(ULONG, AddRef)() { return 1; }
  STDMETHOD_(ULONG, Release)() { return 1; }
  STDMETHOD(QueryInterface)(REFIID iid, LPVOID* ppvObj) {
    if(!ppvObj)
      return E_POINTER;

    *ppvObj = NULL;

    if(IsEqualIID(iid, __uuidof(IUnknown)) ||
        IsEqualIID(iid, __uuidof(IDispatch)) ||
        IsEqualIID(iid, (DIID_DWebBrowserEvents2))) {
      *ppvObj = this;
      return S_OK;
    }

    return E_NOINTERFACE;
  }

  // IDispatch methods
  STDMETHOD(GetTypeInfoCount)(UINT* /*pctinfo*/) {
    ATLTRACENOTIMPL(_T("DWebBrowserEvent2Impl::GetTypeInfoCount"));
  }

  STDMETHOD(GetTypeInfo)(UINT /*iTInfo*/, LCID /*lcid*/, ITypeInfo** ppTInfo) {
    *ppTInfo = NULL;
    ATLTRACENOTIMPL(_T("DWebBrowserEvent2Impl::GetTypeInfo"));
  }

  STDMETHOD(GetIDsOfNames)(REFIID /*riid*/, OLECHAR** /*rgszNames*/, UINT /*cNames*/, LCID /*lcid*/, DISPID* /*rgDispId*/) {
    ATLTRACENOTIMPL(_T("DWebBrowserEvent2Impl::GetIDsOfNames"));
  }

  STDMETHOD(Invoke)(DISPID dispIdMember, REFIID /*riid*/, LCID /*lcid*/, WORD wFlags, DISPPARAMS* pdispparams, VARIANT* /*pVarResult*/, EXCEPINFO* /*pExcepInfo*/, UINT* /*puArgErr*/) {
    ATLASSERT(wFlags == DISPATCH_METHOD);

    if(dispIdMember == DISPID_BEFORENAVIGATE2) {
      // BeforeNavigate2(LPDISPATCH pDisp, VARIANT FAR* URL, VARIANT FAR* Flags, VARIANT FAR* TargetFrameName, VARIANT FAR* PostData, VARIANT FAR* Headers, BOOL FAR* Cancel)
      ATLASSERT(pdispparams->cArgs == 7);
      int npdisp = 6;
      int nURL = 5;
      ATLASSERT(V_VT(&pdispparams->rgvarg[npdisp]) == VT_DISPATCH && V_VT(&pdispparams->rgvarg[nURL]) == (VT_VARIANT | VT_BYREF));
      (static_cast<T*>(this))->_OnBeforeNavigate2(pdispparams->rgvarg[npdisp].pdispVal, pdispparams->rgvarg[nURL].pvarVal);

    } else if(dispIdMember == DISPID_NAVIGATECOMPLETE2) {
      // NavigateComplete2(LPDISPATCH pDisp, VARIANT FAR* URL)
      ATLASSERT(pdispparams->cArgs == 2);
      int npdisp = 1;
      int nURL = 0;
      ATLASSERT(V_VT(&pdispparams->rgvarg[npdisp]) == VT_DISPATCH && V_VT(&pdispparams->rgvarg[nURL]) == (VT_VARIANT | VT_BYREF));
      (static_cast<T*>(this))->_OnNavigateComplete2(pdispparams->rgvarg[npdisp].pdispVal, pdispparams->rgvarg[nURL].pvarVal);

    } else if(dispIdMember == DISPID_DOCUMENTCOMPLETE) {
      // DocumentComplete(LPDISPATCH pDisp, VARIANT* URL)
      ATLASSERT(pdispparams->cArgs == 2);
      int npdisp = 1;
      int nURL = 0;
      ATLASSERT(V_VT(&pdispparams->rgvarg[npdisp]) == VT_DISPATCH && V_VT(&pdispparams->rgvarg[nURL]) == (VT_VARIANT | VT_BYREF));
      (static_cast<T*>(this))->_OnDocumentComplete(pdispparams->rgvarg[npdisp].pdispVal, pdispparams->rgvarg[nURL].pvarVal);
    }

    return S_OK;
  }

  HRESULT ConnectToWebBrowser(IWebBrowser2* pBrowser) {
    if(pBrowser == NULL)
      return E_POINTER;

    HRESULT hr = S_OK;

    if(m_dwCookie == 0) {
      hr = AtlAdvise(pBrowser, this, DIID_DWebBrowserEvents2, &m_dwCookie);
    }

    return hr;
  }

  HRESULT DisconnectToWebBrowser(IWebBrowser2* pBrowser) {
    if(pBrowser == NULL)
      return E_POINTER;

    HRESULT hr = S_OK;

    if(m_dwCookie != 0) {
      hr = AtlUnadvise(pBrowser, DIID_DWebBrowserEvents2, m_dwCookie);

      if(SUCCEEDED(hr))
        m_dwCookie = 0;
    }

    return hr;
  }

protected:
  DWORD m_dwCookie;
};

template<class T, class TBase = CAxWindow>
class DHtmlViewImpl : public ATL::CWindowImpl<T, TBase>,
  public CDHtmlEventSink,
  public DWebBrowserEvent2Impl<DHtmlViewImpl<T, TBase>> {
public:
  DHtmlViewImpl() {
    _host_ui_flags = DOCHOSTUIFLAG_NO3DBORDER | /*DOCHOSTUIFLAG_SCROLL_NO |*/
                     DOCHOSTUIFLAG_DIALOG | DOCHOSTUIFLAG_THEME |
                     DOCHOSTUIFLAG_FLAT_SCROLLBAR | DOCHOSTUIFLAG_NO3DOUTERBORDER;
  }
public:
  BEGIN_MSG_MAP(DHtmlViewImpl)
    MESSAGE_HANDLER(WM_CREATE, OnCreate)
    MESSAGE_HANDLER(WM_NCDESTROY, OnNcDestroy)    
  END_MSG_MAP()
    
  LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/) {
    DefWindowProc();
    HRESULT ret = QueryControl(&_browser_ptr);
    ret = ConnectToWebBrowser(_browser_ptr);

    CComPtr<IAxWinAmbientDispatch> spHost;
    ret = QueryHost(IID_IAxWinAmbientDispatch, (LPVOID*)&spHost);
    if(SUCCEEDED(ret)) {
      spHost->put_AllowContextMenu(VARIANT_FALSE);
      spHost->put_DocHostFlags(_host_ui_flags);
    }
    return 0;
  }

  LRESULT OnNcDestroy(UINT /*msg_id*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled) {
    bHandled = FALSE;
    DisconnectDHtmlEvents();
    _html_doc_ptr = 0;

    if(_browser_ptr) {
      HRESULT ret = DisconnectToWebBrowser(_browser_ptr);
      if(FAILED(ret)) return 0;
      CComPtr<IOleObject> spObject;
      _browser_ptr->QueryInterface(IID_IOleObject, (void**) &spObject);

      if(spObject != NULL) {
        spObject->Close(OLECLOSE_NOSAVE);
        spObject.Release();
      }
    }

    return 0;
  }

public:
  HRESULT GetDHtmlDocument(IHTMLDocument2** pphtmlDoc) {
    if(pphtmlDoc == NULL) {
      ATLASSERT(FALSE);
      return E_POINTER;
    }

    *pphtmlDoc = NULL;

    if(_html_doc_ptr) {
      *pphtmlDoc = _html_doc_ptr;
      (*pphtmlDoc)->AddRef();
      return S_OK;
    }

    return E_NOINTERFACE;
  }

  void GetCurrentUrl(CComBSTR& szUrl) {
    szUrl = _current_url;
  }

  void OnBeforeNavigate(LPDISPATCH /*pDisp*/, LPCTSTR /*url_str*/) {
  }

  void OnNavigateComplete(LPDISPATCH /*pDisp*/, LPCTSTR /*url_str*/) {
  }

  void OnDocumentComplete(LPDISPATCH /*pDisp*/, LPCTSTR /*url_str*/) {
  }

  BOOL LoadFromResource(LPCTSTR lpszResource) {
    HINSTANCE hInstance = _AtlBaseModule.GetResourceInstance();
    ATLASSERT(hInstance != NULL);

    TCHAR strResourceURL[300] = _T("");
    BOOL bRetVal = TRUE;
    LPTSTR lpszModule = new TCHAR[_MAX_PATH];

    int ret = GetModuleFileName(hInstance, lpszModule, _MAX_PATH);

    if(ret == 0 || ret == _MAX_PATH)
      bRetVal = FALSE;
    else {
      _stprintf_s<300>(strResourceURL, _T("res://%s/%s"), lpszModule, lpszResource);
      Navigate(strResourceURL, 0, 0, 0);
    }

    delete [] lpszModule;
    return bRetVal;
  }

  BOOL LoadFromResource(UINT nRes) {
    HINSTANCE hInstance = _AtlBaseModule.GetResourceInstance();
    ATLASSERT(hInstance != NULL);

    TCHAR strResourceURL[300] = _T("");
    BOOL bRetVal = TRUE;
    LPTSTR lpszModule = new TCHAR[_MAX_PATH];

    if(GetModuleFileName(hInstance, lpszModule, _MAX_PATH)) {
      _stprintf_s<300>(strResourceURL, _T("res://%s/%d"), lpszModule, nRes);
      Navigate(strResourceURL, 0, 0, 0);

    } else
      bRetVal = FALSE;

    delete [] lpszModule;
    return bRetVal;
  }

  void Navigate(LPCTSTR lpszURL, DWORD dwFlags = 0, LPCTSTR lpszTargetFrameName = NULL,
                LPCTSTR lpszHeaders = NULL, LPVOID lpvPostData = NULL,
                DWORD dwPostDataLen = 0) {
    CComBSTR bstrURL = lpszURL;

    LPSAFEARRAY psa = NULL;

    //CComSafeArray<BYTE> saPostData;
    if(lpvPostData != NULL) {
      if(dwPostDataLen == 0)
        dwPostDataLen = lstrlen((LPCTSTR) lpvPostData);

      SAFEARRAYBOUND sab = {dwPostDataLen, 0};
      psa = SafeArrayCreate(VT_UI1, 1, &sab);

      if(psa) {
        SafeArrayLock(psa);
        memcpy(psa->pvData, lpvPostData, dwPostDataLen);
        SafeArrayUnlock(psa);
      }

      //saPostData.Add(dwPostDataLen, (LPCBYTE)lpvPostData);
    }

    CComVariant vFlags((long)dwFlags);
    CComVariant vTargetFrameName(lpszTargetFrameName);
    CComVariant vPostData(psa/*saPostData*/);
    CComVariant vHeaders(lpszHeaders);
    _browser_ptr->Navigate(bstrURL, &vFlags, &vTargetFrameName, &vPostData, &vHeaders);

    if(psa) {
      SafeArrayDestroy(psa);
    }
  }

  template<class Q>
  HRESULT GetElementByClass(LPCTSTR szElementClass, Q** ppElem) {
    if(!ppElem)
      return E_POINTER;

    if(szElementClass == NULL)
      return E_INVALIDARG;

    *ppElem = NULL;

    CComBSTR strElementClass = szElementClass;
    HRESULT hr = S_OK;
    CComPtr<IHTMLDocument2> sphtmlDoc;
    hr = GetDHtmlDocument(&sphtmlDoc);

    if(sphtmlDoc == NULL)
      return hr;

    CComPtr<IHTMLElementCollection> spElemAll;
    hr = sphtmlDoc->get_all(&spElemAll);

    if(spElemAll == NULL)
      return E_NOINTERFACE;

    long len = 0;
    hr = spElemAll->get_length(&len);

    for(long i = 0; i < len; i++) {
      CComPtr<IDispatch> spdisp;
      hr = spElemAll->item(CComVariant(i), CComVariant(i), &spdisp);

      if(spdisp == NULL)
        continue;

      CComQIPtr<IHTMLElement> spElem = spdisp;

      if(spElem == NULL)
        continue;

      CComBSTR className;
      hr = spElem->get_className(&className);

      if(strElementClass == className) {
        hr = spElem.QueryInterface(ppElem);
        break;
      }
    }

    return hr;
  }

  HRESULT GetElementsByTag(LPCTSTR szElementTag, IHTMLElementCollection** ppElemColl) {
    if(!ppElemColl)
      return E_POINTER;

    if(szElementTag == NULL)
      return E_INVALIDARG;

    *ppElemColl = NULL;

    HRESULT hr = S_OK;
    CComPtr<IHTMLDocument2> sphtmlDoc;
    hr = GetDHtmlDocument(&sphtmlDoc);

    if(sphtmlDoc == NULL)
      return hr;

    CComQIPtr<IHTMLDocument3> spDoc3 = sphtmlDoc;

    if(spDoc3 == NULL)
      return E_FAIL;

    CComBSTR strElementTag = szElementTag;
    return spDoc3->getElementsByTagName(strElementTag, ppElemColl);
  }

  template<class Q>
  HRESULT GetElementByTag(LPCTSTR szElementTag, Q** ppElem, long idx = 0) {
    if(ppElem == NULL)
      return E_POINTER;

    *ppElem = NULL;
    HRESULT hr = S_OK;
    CComPtr<IHTMLElementCollection> spElemColl;
    hr = GetElementsByTag(szElementTag, &spElemColl);

    if(spElemColl == NULL)
      return E_NOINTERFACE;

    CComPtr<IDispatch> spdispElem;
    hr = spElemColl->item(CComVariant(idx), CComVariant(idx), &spdispElem);

    if(spdispElem)
      hr = spdispElem.QueryInterface(ppElem);

    return hr;
  }

  HRESULT GetElementsByName(LPCTSTR szElementId, IHTMLElementCollection** ppElemColl) {
    if(!ppElemColl)
      return E_POINTER;

    if(szElementId == NULL)
      return E_INVALIDARG;

    *ppElemColl = NULL;

    HRESULT hr = S_OK;
    CComPtr<IHTMLDocument2> sphtmlDoc;
    hr = GetDHtmlDocument(&sphtmlDoc);

    if(sphtmlDoc == NULL)
      return hr;

    CComQIPtr<IHTMLDocument3> spDoc3 = sphtmlDoc;

    if(spDoc3 == NULL)
      return E_FAIL;

    CComBSTR strElementId = szElementId;
    return spDoc3->getElementsByName(strElementId, ppElemColl);
  }

  template<class Q>
  HRESULT GetElement(LPCTSTR szElementId, Q** ppElem, long idx = 0) {
    if(ppElem == NULL)
      return E_POINTER;

    *ppElem = NULL;
    HRESULT hr = S_OK;
    CComPtr<IHTMLElementCollection> spElemColl;
    hr = GetElementsByName(szElementId, &spElemColl);

    if(spElemColl == NULL)
      return E_NOINTERFACE;

    CComPtr<IDispatch> spdispElem;
    hr = spElemColl->item(CComVariant(idx), CComVariant(idx), &spdispElem);

    if(spdispElem)
      hr = spdispElem.QueryInterface(ppElem);

    return hr;
  }

  BSTR GetElementText(LPCTSTR szElementId) {
    BSTR bstrText = NULL;
    CComPtr<IHTMLElement> sphtmlElem;
    GetElement(szElementId, &sphtmlElem);

    if(sphtmlElem)
      sphtmlElem->get_innerText(&bstrText);

    return bstrText;
  }

  void SetElementText(LPCTSTR szElementId, BSTR bstrText) {
    CComPtr<IHTMLElement> sphtmlElem;
    GetElement(szElementId, &sphtmlElem);

    if(sphtmlElem)
      sphtmlElem->put_innerText(bstrText);
  }

  void SetElementText(IUnknown* punkElem, BSTR bstrText) {
    if(punkElem == NULL)
      return;

    CComPtr<IHTMLElement> sphtmlElem;
    punkElem->QueryInterface(__uuidof(IHTMLElement), (void**) &sphtmlElem);

    if(sphtmlElem != NULL)
      sphtmlElem->put_innerText(bstrText);
  }
  BSTR GetElementHtml(LPCTSTR szElementId) {
    BSTR bstrText = NULL;
    CComPtr<IHTMLElement> sphtmlElem;
    GetElement(szElementId, &sphtmlElem);

    if(sphtmlElem)
      sphtmlElem->get_innerHTML(&bstrText);

    return bstrText;
  }
  void SetElementHtml(LPCTSTR szElementId, BSTR bstrText) {
    CComPtr<IHTMLElement> sphtmlElem;
    GetElement(szElementId, &sphtmlElem);

    if(sphtmlElem)
      sphtmlElem->put_innerHTML(bstrText);
  }
  void SetElementHtml(IUnknown* punkElem, BSTR bstrText) {
    if(punkElem == NULL)
      return;

    CComPtr<IHTMLElement> sphtmlElem;
    punkElem->QueryInterface(__uuidof(IHTMLElement), (void**) &sphtmlElem);

    if(sphtmlElem != NULL)
      sphtmlElem->put_innerHTML(bstrText);
  }
  VARIANT GetElementProperty(LPCTSTR szElementId, DISPID dispid) {
    VARIANT varRet;
    CComPtr<IDispatch> spdispElem;
    varRet.vt = VT_EMPTY;
    GetElement(szElementId, &spdispElem);

    if(spdispElem) {
      DISPPARAMS dispparamsNoArgs = { NULL, NULL, 0, 0 };
      spdispElem->Invoke(dispid, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYGET, &dispparamsNoArgs, &varRet, NULL, NULL);
    }

    return varRet;
  }
  void SetElementProperty(LPCTSTR szElementId, DISPID dispid, VARIANT* pVar) {
    CComPtr<IDispatch> spdispElem;
    GetElement(szElementId, &spdispElem);

    if(spdispElem) {
      DISPPARAMS dispparams = {NULL, NULL, 1, 1};
      dispparams.rgvarg = pVar;
      DISPID dispidPut = DISPID_PROPERTYPUT;
      dispparams.rgdispidNamedArgs = &dispidPut;
      spdispElem->Invoke(dispid, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYPUT, &dispparams, NULL, NULL, NULL);
    }
  }
  BOOL HasDialogProperty(LPCTSTR szDlgPropertyName) {
    VARIANT varRet;
    VariantInit(&varRet);
    CComPtr<IHTMLDocument2> spDoc;
    GetDHtmlDocument(&spDoc);

    if(spDoc == NULL)
      return FALSE;

    CComPtr<IHTMLElement> spBody;
    spDoc->get_body(&spBody);

    if(spBody == NULL)
      return FALSE;

    ATLASSERT(szDlgPropertyName);
    CComBSTR strName = szDlgPropertyName;
    HRESULT hr = spBody->getAttribute(strName, 0, &varRet);
    return SUCCEEDED(hr);
  }
  VARIANT GetDialogPropertyInDoc(LPCTSTR szDlgPropertyName) {
    VARIANT varRet;
    VariantInit(&varRet);
    CComPtr<IHTMLDocument2> spDoc;
    GetDHtmlDocument(&spDoc);

    if(spDoc == NULL)
      return varRet;

    CComPtr<IHTMLElement> spBody;
    spDoc->get_body(&spBody);

    if(spBody == NULL)
      return varRet;

    ATLASSERT(szDlgPropertyName);
    CComBSTR strName = szDlgPropertyName;
    HRESULT hr = spBody->getAttribute(strName, 0, &varRet);

    if(FAILED(hr)) {
      V_VT(&varRet) = VT_ERROR;
      V_ERROR(&varRet) = hr;
    }

    return varRet;
  }
  long GetDialogPropertyLong(LPCTSTR szDlgPropertyName, long lDefaultOnFail = 0) {
    CComVariant vRet = GetDialogPropertyInDoc(szDlgPropertyName);

    if(V_VT(&vRet) == VT_EMPTY || V_VT(&vRet) == VT_NULL || V_VT(&vRet) == VT_ERROR)
      return lDefaultOnFail;

    if(V_VT(&vRet) == VT_I4 || SUCCEEDED(vRet.ChangeType(VT_I4)))
      return V_I4(&vRet);

    return lDefaultOnFail;
  }
  BOOL GetDialogPropertyBool(LPCTSTR szDlgPropertyName, BOOL bDefaultOnFail = FALSE) {
    CComVariant vRet = GetDialogPropertyInDoc(szDlgPropertyName);

    if(V_VT(&vRet) == VT_EMPTY || V_VT(&vRet) == VT_NULL || V_VT(&vRet) == VT_ERROR)
      return bDefaultOnFail;

    if(V_VT(&vRet) == VT_BOOL || SUCCEEDED(vRet.ChangeType(VT_BOOL)))
      return (V_BOOL(&vRet) != VARIANT_FALSE);

    return bDefaultOnFail;
  }
  CComBSTR GetDialogPropertyBSTR(LPCTSTR szDlgPropertyName, LPCTSTR szDefaultOnFail = NULL) {
    CComBSTR strRet;

    if(szDefaultOnFail)
      strRet = szDefaultOnFail;

    CComVariant vRet = GetDialogPropertyInDoc(szDlgPropertyName);

    if(V_VT(&vRet) == VT_EMPTY || V_VT(&vRet) == VT_NULL || V_VT(&vRet) == VT_ERROR)
      return strRet;

    if(V_VT(&vRet) == VT_BSTR || SUCCEEDED(vRet.ChangeType(VT_BSTR)))
      strRet = V_BSTR(&vRet);

    return strRet;
  }
  HRESULT GetControlDispatch(LPCTSTR szId, IDispatch** ppdisp) {
    HRESULT hr = S_OK;
    CComPtr<IDispatch> spdispElem;
    hr = GetElement(szId, &spdispElem);

    if(spdispElem) {
      CComPtr<IHTMLObjectElement> sphtmlObj;
      hr = spdispElem.QueryInterface(&sphtmlObj);

      if(sphtmlObj) {
        spdispElem.Release();
        hr = sphtmlObj->get_object(ppdisp);
      }
    }

    return hr;
  }
  VARIANT GetControlProperty(IDispatch* pdispControl, DISPID dispid) {
    VARIANT varRet;
    varRet.vt = VT_EMPTY;

    if(pdispControl) {
      DISPPARAMS dispparamsNoArgs = { NULL, NULL, 0, 0 };
      pdispControl->Invoke(dispid, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYGET, &dispparamsNoArgs, &varRet, NULL, NULL);
    }

    return varRet;
  }
  VARIANT GetControlProperty(LPCTSTR szId, DISPID dispid) {
    CComPtr<IDispatch> spdispElem;
    GetControlDispatch(szId, &spdispElem);
    return GetControlProperty(spdispElem, dispid);
  }
  VARIANT GetControlProperty(LPCTSTR szId, LPCTSTR szPropName) {
    CComVariant varEmpty;
    CComPtr<IDispatch> spdispElem;
    GetControlDispatch(szId, &spdispElem);

    if(!spdispElem)
      return varEmpty;

    DISPID dispid;
    USES_CONVERSION;
    LPOLESTR pPropName = (LPOLESTR)T2COLE(szPropName);
    HRESULT hr = spdispElem->GetIDsOfNames(IID_NULL, &pPropName, 1, LOCALE_USER_DEFAULT, &dispid);

    if(SUCCEEDED(hr))
      return GetControlProperty(spdispElem, dispid);

    return varEmpty;
  }
  void SetControlProperty(IDispatch* pdispControl, DISPID dispid, VARIANT* pVar) {
    if(pdispControl != NULL) {
      DISPPARAMS dispparams = {NULL, NULL, 1, 1};
      dispparams.rgvarg = pVar;
      DISPID dispidPut = DISPID_PROPERTYPUT;
      dispparams.rgdispidNamedArgs = &dispidPut;
      pdispControl->Invoke(dispid, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYPUT, &dispparams, NULL, NULL, NULL);
    }
  }
  void SetControlProperty(LPCTSTR szElementId, DISPID dispid, VARIANT* pVar) {
    CComPtr<IDispatch> spdispElem;
    GetControlDispatch(szElementId, &spdispElem);
    SetControlProperty(spdispElem, dispid, pVar);
  }
  void SetControlProperty(LPCTSTR szElementId, LPCTSTR szPropName, VARIANT* pVar) {
    CComPtr<IDispatch> spdispElem;
    GetControlDispatch(szElementId, &spdispElem);

    if(!spdispElem)
      return;

    DISPID dispid;
    USES_CONVERSION;
    LPOLESTR pPropName = (LPOLESTR)T2COLE(szPropName);
    HRESULT hr = spdispElem->GetIDsOfNames(IID_NULL, &pPropName, 1, LOCALE_USER_DEFAULT, &dispid);

    if(SUCCEEDED(hr))
      SetControlProperty(spdispElem, dispid, pVar);
  }
  HRESULT GetEvent(IHTMLEventObj** ppEventObj) {
    if(ppEventObj == NULL)
      return E_POINTER;

    CComPtr<IHTMLWindow2> sphtmlWnd;
    CComPtr<IHTMLDocument2> sphtmlDoc;
    *ppEventObj = NULL;
    HRESULT hr = GetDHtmlDocument(&sphtmlDoc);

    if(sphtmlDoc == NULL)
      return hr;

    hr = sphtmlDoc->get_parentWindow(&sphtmlWnd);

    if(FAILED(hr))
      goto Error;

    hr = sphtmlWnd->get_event(ppEventObj);

Error:
    return hr;
  }

public:
  void ResizeWindow(long width = 0) {
    static long oldcx = 300;
    static long oldcy = 200;

    // recalc window size
    CComPtr<IHTMLDocument2> spDoc;
    GetDHtmlDocument(&spDoc);

    if(spDoc == NULL)
      return;

    HRESULT hr = S_OK;
    CComQIPtr<IHTMLDocument3> spDoc3 = spDoc;
    CComPtr<IHTMLElement> spDocElem;
    hr = spDoc3->get_documentElement(&spDocElem);
    CComQIPtr<IHTMLElement2> spDocElem2 = spDocElem;

    CComPtr<IHTMLElement> spBody;
    hr = spDoc->get_body(&spBody);
    CComQIPtr<IHTMLElement2> spBody2 = spBody;
    long cx = 300, cy = 200;
    BOOL bScroll = FALSE;

    if(spBody2) {
      if(width <= 0) { // calc real width
        cx = GetDialogPropertyLong(_T("dlg_width"));

        if(cx <= 0)
          hr = spBody2->get_scrollWidth(&cx);

      } else { // width>0, set fixed width
        cx = width;
      }

      long _cy = GetDialogPropertyLong(_T("dlg_height"));

      if(_cy <= 0) {
        // resize webbrowser control, recalc height
        RECT rcBrowser;
        GetWindowRect(&rcBrowser);
        rcBrowser.right = rcBrowser.left + cx;
        MoveWindow(&rcBrowser);
        hr = spBody2->get_scrollHeight(&cy);
        cy += 20; // scrollbar height
        bScroll = TRUE;
        //long cheight;
        ////hr = spDocElem2->get_clientHeight(&cheight);
        ////hr = spDocElem->get_offsetHeight(&cheight);
        ////hr = spDocElem2->get_scrollHeight(&cheight);
        //hr = spBody2->get_clientHeight(&cheight);
        //cy = max(cy, cheight);

      } else
        cy = _cy;
    }

    RECT rc = {0, 0, 0, 0};
    AdjustWindowRectEx(&rc, GetWindowLong(GWL_STYLE), GetMenu() != NULL, GetWindowLong(GWL_EXSTYLE));
    cx += rc.right - rc.left;
    cy += rc.bottom - rc.top;
    RECT rcScreen;
    ::SystemParametersInfo(SPI_GETWORKAREA, 0, &rcScreen, 0);
    cx = min(cx, rcScreen.right - rcScreen.left);
    cy = min(cy, rcScreen.bottom - rcScreen.top);

    RECT rcWin, rcLast;
    GetWindowRect(&rcWin);
    GetWindowRect(&rcLast);
    InflateRect(&rcLast, (cx - (rcLast.right - rcLast.left)) / 2, (cy - (rcLast.bottom - rcLast.top)) / 2);
    long hspan = ((cx - (rcWin.right - rcWin.left)) / 2) / 10;
    long vspan = ((cy - (rcWin.bottom - rcWin.top)) / 2) / 10;

    for(int i = 0; i < 10; i++) {
      DWORD dwBegin = GetTickCount();
      InflateRect(&rcWin, hspan, vspan);
      MoveWindow(&rcWin);
      Sleep(max((20 - (GetTickCount() - dwBegin)), 0));
    }

    MoveWindow(&rcLast);

    if(oldcx == cx && oldcy == cy)
      SendMessage(WM_SIZE);

    oldcx = cx;
    oldcy = cy;
  }

  void _OnBeforeNavigate2(LPDISPATCH pDisp, VARIANT FAR* URL) {
    if(pDisp != _browser_ptr)
      return;

    // ignore script navigation
    ATLASSERT(V_VT(URL) == VT_BSTR);
    CComBSTR strUrl(11, V_BSTR(URL));

    if(lstrcmpiW(strUrl, L"javascript:") == 0)  // is script navigation
      return;

    DisconnectDHtmlEvents();
    _html_doc_ptr = NULL;
    _current_url.Empty();

    USES_CONVERSION;
    ((T*)(this))->OnBeforeNavigate(pDisp, W2CT(V_BSTR(URL)));
  }

  void _OnNavigateComplete2(LPDISPATCH pDisp, VARIANT FAR* URL) {
    if(pDisp != _browser_ptr)
      return;

    IDispatch* pdispDoc = NULL;
    _browser_ptr->get_Document(&pdispDoc);

    if(!pdispDoc)
      return;

    ATLASSERT(_html_doc_ptr == NULL);
    USES_CONVERSION;

    HRESULT ret = pdispDoc->QueryInterface(IID_IHTMLDocument2, (void**) &_html_doc_ptr);
    if(FAILED(ret)) return;

    if(_use_html_title) {
      CComBSTR bstrTitle;
      if(_html_doc_ptr) {
        _html_doc_ptr->get_title(&bstrTitle);
        SetWindowText(OLE2CT((BSTR)bstrTitle));
      }
    }

    ATLASSERT(V_VT(URL) == VT_BSTR);
    _current_url = V_BSTR(URL);

    ConnectDHtmlEvents(pdispDoc);
    pdispDoc->Release();

    (static_cast<T*>(this))->OnNavigateComplete(pDisp, W2CT(V_BSTR(URL)));
  }

  void _OnDocumentComplete(LPDISPATCH pDisp, VARIANT* URL) {
    if(pDisp != _browser_ptr)
      return;

    ConnectDHtmlElementEvents((((DWORD_PTR)static_cast<CDHtmlSinkHandler* >(this)) - (DWORD_PTR)this));

    USES_CONVERSION;
    CComPtr<IHTMLDocument2> spDoc;
    GetDHtmlDocument(&spDoc);

    if(spDoc) {
      CComBSTR strTitle;
      spDoc->get_title(&strTitle);

      if(strTitle.Length() > 0)
        SetWindowText(W2CT(strTitle));
    }

    /* if(HasDialogProperty(_T("dlg_autosize")))
    ResizeWindow();
    */
    ATLASSERT(V_VT(URL) == VT_BSTR);
    //USES_CONVERSION;
    (static_cast<T*>(this))->OnDocumentComplete(pDisp, W2CT(V_BSTR(URL)));
  }

  //Implementation
public:
  HRESULT OnDocumentReadyStateChange(IHTMLElement* phtmlElem) {
    phtmlElem; // unused

    CComPtr<IHTMLDocument2> sphtmlDoc;
    GetDHtmlDocument(&sphtmlDoc);

    if(sphtmlDoc) {
      CComBSTR bstrState;
      sphtmlDoc->get_readyState(&bstrState);

      if(bstrState) {
        if(bstrState == TEXT("complete"))
          ConnectDHtmlElementEvents((((DWORD_PTR)static_cast< CDHtmlSinkHandler* >(this)) - (DWORD_PTR) this));
        else if(bstrState == TEXT("loading"))
          DisconnectDHtmlElementEvents();
      }
    }

    return S_OK;
  }
protected:
  ATL::CComPtr<IWebBrowser2> _browser_ptr;
  ATL::CComPtr<IHTMLDocument2> _html_doc_ptr;
  CComBSTR _current_url;
  LPTSTR _html_res_id_str;
  UINT _html_res_id;
  BOOL _use_html_title;
  DWORD _host_ui_flags;

protected:
  // event sink
  DWORD m_dwDHtmlEventSinkCookie;
  ATL::CSimpleArray<CDHtmlControlSink*> m_ControlSinks;
  ATL::CSimpleArray<CDHtmlElementEventSink*> m_SinkedElements;

  virtual const DHtmlEventMapEntry* GetDHtmlEventMap() { return NULL; }

  HRESULT ElementSink(IDispatch* pdispElement) {
    if(pdispElement && !IsSinkedElement(pdispElement)) {
      HRESULT hr;
      CDHtmlElementEventSink* pSink = NULL;
      ATLTRY(pSink = new CDHtmlElementEventSink(this, pdispElement));

      if(pSink == NULL)
        return E_OUTOFMEMORY;

      hr = AtlAdvise(pdispElement, pSink, __uuidof(IDispatch), &pSink->m_dwCookie);

      if(SUCCEEDED(hr))
        m_SinkedElements.Add(pSink);
      else
        delete pSink;

#ifdef _DEBUG

      if(FAILED(hr))
        ATLTRACE(_T("Warning: Failed to connect to ConnectionPoint!\n"));

#endif
    }

    return S_OK;
  }

  HRESULT ConnectDHtmlEvents(IUnknown* punkDoc) {
    return ConnectToConnectionPoint(punkDoc, __uuidof(HTMLDocumentEvents), &m_dwDHtmlEventSinkCookie);
  }

  void DisconnectDHtmlEvents() {
    CComPtr<IHTMLDocument2> sphtmlDoc;
    GetDHtmlDocument(&sphtmlDoc);

    if(sphtmlDoc == NULL)
      return;

    DisconnectFromConnectionPoint(sphtmlDoc, __uuidof(HTMLDocumentEvents), m_dwDHtmlEventSinkCookie);
    DisconnectDHtmlElementEvents();
  }

  BOOL FindSinkForObject(LPCTSTR szName) {
    if(!szName)
      return TRUE;

    int nLength = m_ControlSinks.GetSize();

    for(int i = 0; i < nLength; i++) {
      if(!_tcscmp(szName, m_ControlSinks[i]->m_szControlId))
        return TRUE;
    }

    return FALSE;
  }

  BOOL IsSinkedElement(IDispatch* pdispElem) {
    if(!pdispElem)
      return TRUE;

    CComPtr<IUnknown> spunk;
    pdispElem->QueryInterface(__uuidof(IUnknown), (void**) &spunk);

    if(!spunk)
      return FALSE;

    for(int i = 0; i < m_SinkedElements.GetSize(); i++) {
      if(spunk == m_SinkedElements[i]->m_spunkElem)
        return TRUE;
    }

    return FALSE;
  }

  HRESULT ConnectDHtmlElementEvents(DWORD_PTR dwThunkOffset = 0) {
    HRESULT hr = S_OK;
    const DHtmlEventMapEntry* pEventMap = GetDHtmlEventMap();

    if(!pEventMap)
      return hr;

    for(int i = 0; pEventMap[i].nType != DHTMLEVENTMAPENTRY_END; i++) {
      //if (pEventMap[i].nType!=DHTMLEVENTMAPENTRY_CONTROL)
      if(pEventMap[i].nType == DHTMLEVENTMAPENTRY_ELEMENT) {
        // an element name must be specified when using element events
        ATLASSERT(pEventMap[i].szName);

        // connect to the element's event sink
        CComPtr<IDispatch> spdispElement;
        GetElement(pEventMap[i].szName, &spdispElement);
        ElementSink(spdispElement);

      } else if(pEventMap[i].nType == DHTMLEVENTMAPENTRY_CONTROL) {
        // check if we already have a sink connected to this control
        if(!FindSinkForObject(pEventMap[i].szName)) {
          // create a new sink and
          // connect it to the element's event sink
          CComPtr<IDispatch> spdispElement;
          GetElement(pEventMap[i].szName, &spdispElement);

          if(spdispElement) {
            CComPtr<IHTMLObjectElement> sphtmlObj;
            spdispElement->QueryInterface(__uuidof(IHTMLObjectElement), (void**) &sphtmlObj);

            if(sphtmlObj) {
              CComPtr<IDispatch> spdispControl;
              sphtmlObj->get_object(&spdispControl);

              if(spdispControl) {
                // create a new control sink to connect to the control's events
                CDHtmlControlSink* pSink = NULL;
                ATLTRY(pSink = new CDHtmlControlSink(spdispControl, this, pEventMap[i].szName, dwThunkOffset));

                if(pSink == NULL)
                  return E_OUTOFMEMORY;

                m_ControlSinks.Add(pSink);
              }
            }
          }
        }
      }
    }

    return hr;
  }

  void DisconnectDHtmlElementEvents() {
    const DHtmlEventMapEntry* pEventMap = GetDHtmlEventMap();

    if(!pEventMap)
      return;

    int i;

    // disconnect from element events
    for(i = 0; i < m_SinkedElements.GetSize(); i++) {
      CDHtmlElementEventSink* pSink = m_SinkedElements[i];
      AtlUnadvise(pSink->m_spunkElem, __uuidof(IDispatch), pSink->m_dwCookie);
      delete pSink;
    }

    m_SinkedElements.RemoveAll();

    // disconnect from control events
    for(i = 0; i < m_ControlSinks.GetSize(); i++) {
      DisconnectFromConnectionPoint(m_ControlSinks[i]->m_spunkObj,
                                    m_ControlSinks[i]->m_iid, m_ControlSinks[i]->m_dwCookie);
      delete m_ControlSinks[i];
    }

    m_ControlSinks.RemoveAll();
    return;
  }
};

}


#endif // __ATL_DHTML_H__
