#ifndef __CTRL_ANIMATION_H__
#define __CTRL_ANIMATION_H__

#include <stdint.h>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <algorithm>
#include <windows.h>
#include <atlbase.h>
#include <atlapp.h>
#include <atlmisc.h>

class AnimationHost;
class AnimationWidget {
public:
  AnimationWidget() : _animating(false), _host(0) {}
  virtual bool CalculateUpdate(HWND& hwnd, RECT &rect) = 0;
  int32_t Initialize(AnimationHost* host) { _host = host; return 0; }
public:
  HWND _host_wnd;
  AnimationHost *_host;
  CRect _animation_rect;
  bool _animating;
};


class AnimationHost {
public:
  enum { ANIMATION_TIMERID = 42 };

public:
  AnimationHost() : _timer_id(0), _parent_wnd(0) {}

  int32_t Initialize(HWND wnd) {
    _parent_wnd = wnd;
    return 0;
  }

  BEGIN_MSG_MAP(AnimationHost)
    MESSAGE_HANDLER(WM_TIMER, OnTimer)
  END_MSG_MAP()

  LRESULT OnTimer(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled) {
    if(wParam != _timer_id) {
      bHandled = FALSE;
      return 0;
    }

    if(false == CalculateUpdate()) {
      //LOG_DEBUG(DBG, "stop animation timer(%u)", _timer_id);
      KillTimer(_parent_wnd, _timer_id);    
      _timer_id = 0;
    }

    auto itor = _update_rects.begin();
    while (itor != _update_rects.end()) {
      InvalidateRect(itor->first, &itor->second, FALSE);
      ++ itor;
    }
    return 0;
  }

  int32_t StartAnimation(AnimationWidget* widget) {
    if(0 == widget) return -1;
    auto itor = std::find(_act_list.begin(), _act_list.end(), widget);
    if(itor != _act_list.end()) {
      return 0;
    }

    _act_list.push_back(widget);

    if(0 == _timer_id) {
      _timer_id = SetTimer(_parent_wnd, ANIMATION_TIMERID, 25, 0);
      //LOG_TRACE(DBG, "start animation timer(%u)", _timer_id);
    }
    return 0;
  }

protected:
  bool CalculateUpdate() {
    _update_rects.clear();
    std::set<AnimationWidget*> removal;

    bool still_animating = false;
    auto itor = _act_list.begin();
    while(itor != _act_list.end()) {
      HWND wnd = 0;
      CRect rc;
      if((*itor)->CalculateUpdate(wnd, rc)) {
        still_animating = true;

        if(wnd) {
          auto itor = _update_rects.find(wnd);
          CRect rect_org;
          if(itor != _update_rects.end())
            rect_org = itor->second;
          rect_org.UnionRect(rect_org, rc);
          _update_rects[wnd] = rect_org;
        }
      } else {
        removal.insert((*itor));
      }
      ++ itor;
    }

    // remove animation widgets which doesn't animating.
    for(auto i = removal.begin(); i != removal.end(); ++ i) {
      _act_list.remove(*i);
    }
    return still_animating;
  }

protected:
  std::list<AnimationWidget*> _act_list;
  std::map<HWND, RECT> _update_rects;
  uint32_t _timer_id;
  HWND _parent_wnd;
};

#endif // __CTRL_ANIMATION_H__
