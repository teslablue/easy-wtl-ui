#ifndef __RESOURCE_MGR_H__
#define __RESOURCE_MGR_H__

#pragma once

#include <stdint.h>
#include <vector>
#include <windows.h>
#include <map>
#include "img_helpers.h"

class ResourceMgr {
public:
  ResourceMgr(void) {}
  virtual ~ResourceMgr(void) {
    Destroy();
  }

  int32_t Initialize() {
    return 0;
  }

  void Destroy() {
    auto itor = _res_map.begin();
    while(itor != _res_map.end()) {
      DeleteObject(itor->second);
      ++ itor;
    }
    _res_map.clear();

    for(uint32_t i = 0; i < _res_array.size(); ++ i) {
      FreeLibrary(_res_array[i]);
    }
    _res_array.clear();
  }

  int32_t AddResource(const TCHAR* path) {
    HMODULE res_inst = LoadLibrary(path);
    if(0 == res_inst) return -1;

    _res_array.push_back(res_inst);
    return 0;
  }

  int32_t LoadResource(uint32_t res_id, const TCHAR* type) {
    if(_res_array.size() <= 0) return -1;
    for(uint32_t i = 0; i < _res_array.size(); ++ i) {
      HBITMAP obj = AtlLoadGdiplusImage(_res_array[0], res_id, type);
      if(0 == obj) return -1;

      InsertResouece(res_id, obj);
      break;
    }

    return 0;
  }

  int32_t LoadFont(uint32_t res_id) {
    CLogFont lf;
    lf.SetMenuFont();
    lf.MakeLarger(2);

    CFont font;
    if(FALSE == font.CreateFontIndirect(&lf)) {
      return -1;
    }

    HFONT obj = font.Detach();
    InsertResouece(res_id, obj);
    return 0;
  }

  HBITMAP GetBitmap(uint32_t res_id) const {
    HGDIOBJ obj = 0;
    auto itor = _res_map.find(res_id);
    if(itor == _res_map.end())
      return 0;
    obj = itor->second;
    if(OBJ_BITMAP == GetObjectType(obj))
      return (HBITMAP)obj;
    else
      return 0;
  }

protected:
  void InsertResouece(uint32_t res_id, HGDIOBJ obj) {
    auto itor = _res_map.find(res_id);
    if(itor != _res_map.end()) {
      HGDIOBJ old_obj = itor->second;
      _res_map.erase(itor);
      DeleteObject(old_obj);
    }
    _res_map[res_id] = obj;
  }

private:
  std::vector<HMODULE> _res_array;
  std::map<uint32_t, HGDIOBJ> _res_map;
};

#endif // __RESOURCE_MGR_H__
