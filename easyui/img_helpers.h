#if !defined(_WTL_IMAGEHELPERS_)
#define _WTL_IMAGEHELPERS_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <atlbase.h>
#include <atlapp.h>
#include <atlmisc.h>
#include <gdiplus.h>
#pragma comment(lib, "gdiplus.lib")

class CResourceEx {
public:
  HGLOBAL m_hGlobal;
  HRSRC m_hResource;
  HMODULE m_hResHandle;
  // Constructor/destructor
  CResourceEx(HMODULE inst) : m_hGlobal(NULL), m_hResource(NULL),
    m_hResHandle(inst) {
  }

  ~CResourceEx() {
    Release();
  }

  // Load methods
  bool Load(ATL::_U_STRINGorID Type, ATL::_U_STRINGorID ID) {
    ATLASSERT(m_hResource == NULL);
    ATLASSERT(m_hGlobal == NULL);

    m_hResource = ::FindResource(m_hResHandle, ID.m_lpstr, Type.m_lpstr);
    if(m_hResource == NULL)
      return false;

    m_hGlobal = ::LoadResource(m_hResHandle, m_hResource);
    if(m_hGlobal == NULL) {
      m_hResource = NULL;
      return false;
    }

    return true;
  }

#ifndef _WIN32_WCE
  bool LoadEx(ATL::_U_STRINGorID Type, ATL::_U_STRINGorID ID, WORD wLanguage) {
    ATLASSERT(m_hResource == NULL);
    ATLASSERT(m_hGlobal == NULL);

    m_hResource = ::FindResourceEx(m_hResHandle, ID.m_lpstr, Type.m_lpstr, wLanguage);
    if(m_hResource == NULL)
      return false;

    m_hGlobal = ::LoadResource(m_hResHandle, m_hResource);
    if(m_hGlobal == NULL) {
      m_hResource = NULL;
      return false;
    }

    return true;
  }
#endif // !_WIN32_WCE

  // Misc. operations
  DWORD GetSize() const {
    ATLASSERT(m_hResource != NULL);
    return ::SizeofResource(m_hResHandle, m_hResource);
  }

  LPVOID Lock() {
    ATLASSERT(m_hResource != NULL);
    ATLASSERT(m_hGlobal != NULL);
    LPVOID pVoid = ::LockResource(m_hGlobal);
    ATLASSERT(pVoid != NULL);
    return pVoid;
  }

  void Release() {
    if(m_hGlobal != NULL) {
      FreeResource(m_hGlobal);
      m_hGlobal = NULL;
      m_hResource = NULL;
    }
  }
};


ATLINLINE HBITMAP AtlLoadGdiplusImage(HMODULE inst, ATL::_U_STRINGorID bitmap,
                                      ATL::_U_STRINGorID type = (UINT) 0) {
  USES_CONVERSION;
  static bool s_bInitied = false;
  if(!s_bInitied) {
    s_bInitied = true;
    Gdiplus::GdiplusStartupInput gsi;
    Gdiplus::GdiplusStartupOutput gso;
    ULONG uToken = 0;
    Gdiplus::GdiplusStartup(&uToken, &gsi, &gso);
  }
  Gdiplus::Bitmap* pBitmap = NULL;
  if(HIWORD(bitmap.m_lpstr) != NULL) {
    // Load from filename
    pBitmap = new Gdiplus::Bitmap(T2CW(bitmap.m_lpstr));
  } else if(type.m_lpstr != NULL && type.m_lpstr != RT_BITMAP) {
    // Loading PNG, JPG resources etc
    CResourceEx res(inst);
    if(!res.Load(type, bitmap)) return NULL;
    DWORD dwSize = res.GetSize();
    HANDLE hMemory = ::GlobalAlloc(GMEM_MOVEABLE, dwSize);
    if(hMemory == NULL) return NULL;
    ::memcpy(::GlobalLock(hMemory), res.Lock(), dwSize);
    ::GlobalUnlock(hMemory);
    IStream* pStream = NULL;
    if(FAILED(::CreateStreamOnHGlobal(hMemory, TRUE, &pStream))) {
      ::GlobalFree(hMemory);
      return FALSE;
    }
    pBitmap = new Gdiplus::Bitmap(pStream);
    pStream->Release();
  } else {
    // This only loads BMP resources
    pBitmap = new Gdiplus::Bitmap(inst,
                                  (LPCWSTR)(UINT) bitmap.m_lpstr);
  }
  if(pBitmap == NULL) return NULL;
  HBITMAP hBitmap = NULL;
  pBitmap->GetHBITMAP(NULL, &hBitmap);
  delete pBitmap;
  return hBitmap;
}


ATLINLINE HBRUSH AtlGetBackgroundBrush(HWND hWnd, HWND hwndParent) {
  CWindow wnd = hWnd;
  CWindow wndParent = hwndParent;
  CClientDC dcParent = wndParent;
  CRect rcParent;
  wndParent.GetWindowRect(&rcParent);
  CDC dcCompat1;
  dcCompat1.CreateCompatibleDC(dcParent);
  CBitmap bmpCompat1;
  bmpCompat1.CreateCompatibleBitmap(dcParent, rcParent.Width(), rcParent.Height());
  HBITMAP hOldBmp1 = dcCompat1.SelectBitmap(bmpCompat1);
  wndParent.SendMessage(WM_ERASEBKGND, (WPARAM)(HDC) dcCompat1, 0);
  wndParent.SendMessage(WM_PRINTCLIENT, (WPARAM)(HDC) dcCompat1,
                        (LPARAM)(PRF_ERASEBKGND | PRF_CLIENT | PRF_NONCLIENT));
  CRect rcWin;
  wnd.GetWindowRect(&rcWin);
  CDC dcCompat2;
  dcCompat2.CreateCompatibleDC();
  CBitmap bmpCompat2;
  bmpCompat2.CreateCompatibleBitmap(dcCompat1, rcWin.Width(), rcWin.Height());
  HBITMAP hOldBmp2 = dcCompat2.SelectBitmap(bmpCompat2);
  CRect rcSnap = rcWin;
  ::MapWindowPoints(NULL, wndParent, (LPPOINT)(LPRECT) &rcSnap, 2);
  dcCompat2.BitBlt(0, 0, rcWin.Width(), rcWin.Height(), dcCompat1, rcSnap.left, rcSnap.top,
                   SRCCOPY);
  HBRUSH hBrush = ::CreatePatternBrush(bmpCompat2);
  dcCompat1.SelectBitmap(hOldBmp1);
  dcCompat2.SelectBitmap(hOldBmp2);
  return hBrush;
}

enum { IMGDRAWMODE_NORMAL, IMGDRAWMODE_CENTER = 1 };
ATLINLINE VOID AlphaBlendStretch(CDCHandle dc_dest, CRect rc_dest, CDCHandle dc_src,
                                 CRect rc_img, uint8_t fade_factor, CRect rc_corners, uint32_t mode = 0) {
  ATLASSERT(rc_img.Width() > rc_corners.left + rc_corners.right);
  ATLASSERT(rc_img.Height() > rc_corners.top + rc_corners.bottom);
  if (IMGDRAWMODE_CENTER == mode) {
    int32_t x_diff = rc_dest.Width() - rc_img.Width(),
      y_diff = rc_dest.Height() - rc_img.Height();
    if (x_diff > 1) {
      rc_dest.DeflateRect(x_diff / 2, 0);
    }
    if (y_diff > 1) {
      rc_dest.DeflateRect(0, y_diff / 2);
    }
  }
  BLENDFUNCTION bf = { AC_SRC_OVER, 0, fade_factor, AC_SRC_ALPHA };
  // left-top
  dc_dest.AlphaBlend(rc_dest.left, rc_dest.top, rc_corners.left, rc_corners.top, dc_src,
                    rc_img.left, rc_img.top, rc_corners.left, rc_corners.top, bf);
  // top
  dc_dest.AlphaBlend(rc_dest.left + rc_corners.left, rc_dest.top,
                    rc_dest.Width() - rc_corners.left - rc_corners.right, rc_corners.top, dc_src,
                    rc_img.left + rc_corners.left, rc_img.top, rc_img.Width() - rc_corners.left - rc_corners.right,
                    rc_corners.top, bf);
  // right-top
  dc_dest.AlphaBlend(rc_dest.right - rc_corners.right, rc_dest.top, rc_corners.right,
    rc_corners.top, dc_src, rc_img.right - rc_corners.right, rc_img.top, rc_corners.right,
    rc_corners.top, bf);
  // left
  dc_dest.AlphaBlend(rc_dest.left, rc_dest.top + rc_corners.top, rc_corners.left,
                    rc_dest.Height() - rc_corners.top - rc_corners.bottom, dc_src, rc_img.left,
                    rc_img.top + rc_corners.top, rc_corners.left,
                    rc_img.Height() - rc_corners.top - rc_corners.bottom, bf);
  // middle
  dc_dest.AlphaBlend(rc_dest.left + rc_corners.left, rc_dest.top + rc_corners.top,
                    rc_dest.Width() - rc_corners.left - rc_corners.right,
                    rc_dest.Height() - rc_corners.top - rc_corners.bottom, dc_src, rc_img.left + rc_corners.left,
                    rc_img.top + rc_corners.top, rc_img.Width() - rc_corners.left - rc_corners.right,
                    rc_img.Height() - rc_corners.top - rc_corners.bottom, bf);
  // right
  dc_dest.AlphaBlend(rc_dest.right - rc_corners.right, rc_dest.top + rc_corners.top,
                    rc_corners.right, rc_dest.Height() - rc_corners.top - rc_corners.bottom, dc_src,
                    rc_img.right - rc_corners.right, rc_img.top + rc_corners.top, rc_corners.right,
                    rc_img.Height() - rc_corners.top - rc_corners.bottom, bf);
  // left-bottom
  dc_dest.AlphaBlend(rc_dest.left, rc_dest.bottom - rc_corners.bottom, rc_corners.left,
                    rc_corners.bottom, dc_src, rc_img.left, rc_img.bottom - rc_corners.bottom, rc_corners.left,
                    rc_corners.bottom, bf);
  // bottom
  dc_dest.AlphaBlend(rc_dest.left + rc_corners.left, rc_dest.bottom - rc_corners.bottom,
                    rc_dest.Width() - rc_corners.left - rc_corners.right, rc_corners.bottom, dc_src,
                    rc_img.left + rc_corners.left, rc_img.bottom - rc_corners.bottom,
                    rc_img.Width() - rc_corners.left - rc_corners.right, rc_corners.bottom, bf);
  // right-bottom
  dc_dest.AlphaBlend(rc_dest.right - rc_corners.right, rc_dest.bottom - rc_corners.bottom,
                    rc_corners.right, rc_corners.bottom, dc_src, rc_img.right - rc_corners.right,
                    rc_img.bottom - rc_corners.bottom, rc_corners.right, rc_corners.bottom, bf);
}


//ATLINLINE VOID AlphaBlendStretch(CDCHandle dcDest, CRect rcDest, CDCHandle dcSrc,
//  CRect rcBmp, BYTE uFade, CRect rcCorners) {
//    ATLASSERT(rcBmp.Width() > rcCorners.left + rcCorners.right);
//    ATLASSERT(rcBmp.Height() > rcCorners.top + rcCorners.bottom);
//    BLENDFUNCTION bf = { AC_SRC_OVER, 0, uFade, AC_SRC_ALPHA };
//    // left-top
//    dcDest.AlphaBlend(rcDest.left, rcDest.top, rcCorners.left, rcCorners.top, dcSrc,
//      rcBmp.left, rcBmp.top, rcCorners.left, rcCorners.top, bf);
//    // top
//    dcDest.AlphaBlend(rcDest.left + rcCorners.left, rcDest.top,
//      rcDest.Width() - rcCorners.left - rcCorners.right - 1, rcCorners.top, dcSrc,
//      rcBmp.left + rcCorners.left, rcBmp.top, rcBmp.Width() - rcCorners.left - rcCorners.right,
//      rcCorners.top, bf);
//    // right-top
//    dcDest.AlphaBlend(rcDest.right - rcCorners.right - 1, rcDest.top, rcCorners.right,
//      rcCorners.top, dcSrc, rcBmp.right - rcCorners.right, rcBmp.top, rcCorners.right,
//      rcCorners.top, bf);
//    // left
//    dcDest.AlphaBlend(rcDest.left, rcDest.top + rcCorners.top, rcCorners.left,
//      rcDest.Height() - rcCorners.top - rcCorners.bottom, dcSrc, rcBmp.left,
//      rcBmp.top + rcCorners.top, rcCorners.left,
//      rcBmp.Height() - rcCorners.top - rcCorners.bottom, bf);
//    // middle
//    dcDest.AlphaBlend(rcDest.left + rcCorners.left, rcDest.top + rcCorners.top,
//      rcDest.Width() - rcCorners.left - rcCorners.right,
//      rcDest.Height() - rcCorners.top - rcCorners.bottom, dcSrc, rcBmp.left + rcCorners.left,
//      rcBmp.top + rcCorners.top, rcBmp.Width() - rcCorners.left - rcCorners.right,
//      rcBmp.Height() - rcCorners.top - rcCorners.bottom, bf);
//    // right
//    dcDest.AlphaBlend(rcDest.right - rcCorners.right - 1, rcDest.top + rcCorners.top,
//      rcCorners.right, rcDest.Height() - rcCorners.top - rcCorners.bottom, dcSrc,
//      rcBmp.right - rcCorners.right, rcBmp.top + rcCorners.top, rcCorners.right,
//      rcBmp.Height() - rcCorners.top - rcCorners.bottom, bf);
//    // left-bottom
//    dcDest.AlphaBlend(rcDest.left, rcDest.bottom - rcCorners.bottom - 1, rcCorners.left,
//      rcCorners.bottom, dcSrc, rcBmp.left, rcBmp.bottom - rcCorners.bottom, rcCorners.left,
//      rcCorners.bottom, bf);
//    // bottom
//    dcDest.AlphaBlend(rcDest.left + rcCorners.left, rcDest.bottom - rcCorners.bottom - 1,
//      rcDest.Width() - rcCorners.left - rcCorners.right, rcCorners.bottom, dcSrc,
//      rcBmp.left + rcCorners.left, rcBmp.bottom - rcCorners.bottom,
//      rcBmp.Width() - rcCorners.left - rcCorners.right, rcCorners.bottom, bf);
//    // right-bottom
//    dcDest.AlphaBlend(rcDest.right - rcCorners.right - 1, rcDest.bottom - rcCorners.bottom - 1,
//      rcCorners.right, rcCorners.bottom, dcSrc, rcBmp.right - rcCorners.right,
//      rcBmp.bottom - rcCorners.bottom, rcCorners.right, rcCorners.bottom, bf);
//}
#endif // !defined(_WTL_IMAGEHELPERS_)

