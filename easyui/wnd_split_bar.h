#ifndef __WND_SPLITTER_BAR_H__
#define __WND_SPLITTER_BAR_H__

#include <kptree/tree.h>
#include <set>
#include "wnd_defer.h"

typedef void* HPANE;

#define PANE_NAME_LENGTH  128

template <class T>
class SplitterBar {
public:
  BEGIN_MSG_MAP(SplitterBar)
    MESSAGE_HANDLER(WM_SETCURSOR, OnSetCursor)
    MESSAGE_HANDLER(WM_LBUTTONDOWN, OnLButtonDown)
    MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
    MESSAGE_HANDLER(WM_LBUTTONUP, OnLButtonUp)
  END_MSG_MAP()

  LRESULT OnMouseMove(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled) {
    if(_drag_pane) {
      CPoint point(lParam);
      bool horz = IsPaneHorizontal(_drag_pane);
      CPoint ptShift = point - _pt_split_drag_start;
      if(horz) {

      } else {

      }
    }
    bHandled = FALSE;
    return 0;
  }

  LRESULT OnLButtonDown(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled) {
    bHandled = FALSE;
    if(0 == _cur_hit_pane)
      return 0;

    _drag_pane = _cur_hit_pane;
    CPoint point(lParam);
    StartSplitterDragging(point);

    CRect rc;
    GetPaneSplitterBarRect(_cur_hit_pane, rc);
    DrawDragRect(rc, IsPaneHorizontal(_cur_hit_pane));
    return 0;
  }

  LRESULT OnLButtonUp(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled) {
    bHandled = FALSE;
    StopSplitterDragging(false);
    return 0;
  }

  LRESULT OnSetCursor(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled) {
    T* pT = static_cast<T*>(this);
    CPoint pt;
    ::GetCursorPos(&pt);
    pT->ScreenToClient(&pt);

    HPANE pane = 0;
    pane = HitTestSplitter(pt);
    _cur_hit_pane = pane;

    if(pane) {
      if(IsPaneHorizontal(pane))
        SetCursor(LoadCursor(NULL, IDC_SIZENS));
      else
        SetCursor(LoadCursor(NULL, IDC_SIZEWE));
      return 0;
    }

    bHandled = FALSE;
    return 0;
  }

  LRESULT OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled) {
    bHandled = FALSE;
    //RecalcSplitterBar()
    return 0;
  }

public:
  SplitterBar() : _splitter_size(2, 2), _pane_min_size(10, 10), _pane_root(0),
    _cur_hit_pane(0), _drag_pane(0) {
  }

  HPANE HitTestSplitter(CPoint pt, HPANE parent) {
    tree<struct pane_t>::iterator root((tree_node_<struct pane_t>*)parent);
    int cnt = _pane_tree.number_of_children(root);
    for(tree<struct pane_t>::sibling_iterator slibing = _pane_tree.begin(root);
        slibing != _pane_tree.end(root); ++ slibing) {
      HPANE ret = 0;
      if(_pane_tree.number_of_children(slibing)) {
        ret = HitTestSplitter(pt, (HPANE)slibing.node);
        if(ret) return ret;
      }
      if(slibing->real.rect_splitter.PtInRect(pt))
        ret = (HPANE) slibing.node;
      if(ret && 2 == cnt) {
        tree<struct pane_t>::iterator itor = _pane_tree.next_sibling(slibing);
        if(itor != _pane_tree.end(root) && itor->fixed)
          ret = 0;
      }
      return ret;
    }
    return 0;
  }

  HPANE HitTestSplitter(CPoint pt) {
    if(0 == _pane_root) return 0;
    return HitTestSplitter(pt, _pane_root);
    //tree<struct pane_t>::iterator root((tree_node_<struct pane_t>*)_pane_root);
    /*for(tree<struct pane_t>::sibling_iterator slibing = _pane_tree.begin(root);
        slibing != _pane_tree.end(root); ++ slibing) {

      HPANE pane = HitTestSplitter(pt, slibing.node);
      if(_pane_tree.number_of_children(slibing)) {
        DrawSplitter(dc, (HPANE)slibing.node);
      }

      if (slibing->real.rect_splitter.PtInRect(pt)) {
        return (HPANE) slibing.node;
      }
    }*/
    return 0;
  }

  void StartSplitterDragging(CPoint point) {
    T* pT = static_cast<T*>(this);
    _pt_split_drag_start = point;
    pT->SetCapture();
  }

  void StopSplitterDragging(bool /*reset*/) {
    T* pT = static_cast<T*>(this);
    if(0 == _drag_pane) return;

    DrawDragRect(CRect(0, 0, 0, 0), IsPaneHorizontal(_drag_pane));
    _drag_pane = 0;
    if(::GetCapture() == pT->m_hWnd)
      ReleaseCapture();
  }

  int32_t SetDynamicSplitterBar(HPANE pane, bool dynamic) {
    if(!IsPaneExist(pane)) return -1;
    tree<struct pane_t>::iterator pane_itor((tree_node_<struct pane_t>*)pane);
    pane_itor->dynamic_bar = dynamic;
    return 0;
  }

  int32_t SetSplitterBarSize(HPANE pane, CSize size) {
    if(!IsPaneExist(pane)) return 0;
    tree<struct pane_t>::iterator pane_itor((tree_node_<struct pane_t>*)pane);
    pane_itor->real.bar_size = size;
    return 0;
  }

  int32_t SetPaneSize(HPANE pane, uint32_t size, bool fixed, bool recalc = false) {
    if(!IsPaneExist(pane)) return -1;
    tree<struct pane_t>::iterator pane_itor((tree_node_<struct pane_t>*)pane);
    pane_itor->fixed = fixed;
    pane_itor->fixed_size = size;
    if(recalc) {
      WindowDefer wd;
      pane_itor = _pane_tree.parent(pane_itor);
      RecalcSplitterBar(&wd, pane_itor.node, pane_itor->real.rect_pane, true);
    }
    return 0;
  }

  int32_t InitializeSplitterBar(bool horizontal) {
    struct pane_t pane = {0};
    pane.dynamic_bar = true;
    _tcsncpy(pane.name, _T("root"), PANE_NAME_LENGTH);
    tree<struct pane_t>::iterator pos;
    pane.horz = horizontal;
    pos = _pane_tree.insert(_pane_tree.begin(), pane);
    _pane_root = pos.node;
    _pane_sets.insert(_pane_root);
    return 0;
  }

  CSize GetSplitterBarSize() {
    return _splitter_size;
  }

  void SetSplitterBarSize(CSize size) {
    _splitter_size = size;
  }

  void SetPaneSplitterBarSize(HPANE pane, CSize size) {
    if(0 == pane) pane = _pane_root;
    if(!IsPaneExist(pane)) return;
    tree<struct pane_t>::iterator itor((tree_node_<struct pane_t>*)pane);
    itor->real.bar_size = size;
  }

  bool IsPaneHorizontal(HPANE pane) {
    if(0 == pane) return false;

    tree<struct pane_t>::iterator itor((tree_node_<struct pane_t>*)pane);
    return itor->horz;
  }

  bool IsPaneExist(HPANE pane) {
    if(0 == pane) return true;
    auto itor = _pane_sets.find(pane);
    if(itor != _pane_sets.end())
      return true;
    return false;
  }

  HPANE AddPane(HPANE parent, const TCHAR* name = 0, bool horizontal = true) {
    if(0 == parent)
      parent = _pane_root;

    tree<struct pane_t>::iterator parent_itor((tree_node_<struct pane_t>*)parent), pos;
    struct pane_t pane = {0};
    pane.dynamic_bar = true;
    pane.horz = horizontal;
    if(name)
      _tcsncpy(pane.name, name, PANE_NAME_LENGTH);
    HPANE ret = 0;

    pos = _pane_tree.append_child(parent_itor, pane);
    ret = (HPANE) pos.node;

    if(ret)
      _pane_sets.insert(ret);
    return ret;
  }

  bool SetPaneWnd(HPANE pane, HWND wnd) {
    if (!IsWindow(wnd)) return false;

    if(!IsPaneExist(pane)) return false;

    tree<struct pane_t>::iterator itor((tree_node_<struct pane_t>*)pane);
    itor->view_wnd = wnd;

    return true;
  }

  // get the number of panes in the line.
  int GetCount(HPANE parent) const {
    tree<struct pane_t>::iterator itor((tree_node_<struct pane_t>*)parent);
    return _pane_tree.number_of_children(itor);
  }

  void GetPaneSplitterBarRect(HPANE pane, CRect &rect) {
    rect.SetRectEmpty();

    if(!IsPaneExist(pane)) return;

    tree<struct pane_t>::iterator itor((tree_node_<struct pane_t>*)pane);
    rect = itor->real.rect_splitter;
  }

  // set equal size (width for horizontal and height for vertical) for all child panes of 'parent'.
  void SetEqualPaneSize(HPANE parent) {
    if(0 == parent) return;

    int children_cnt = GetCount(parent);
    if(0 == children_cnt) return;
    tree<struct pane_t>::iterator root((tree_node_<struct pane_t>*)parent);
    tree<struct pane_t>::sibling_iterator children;
    for(children = _pane_tree.begin(root); children != _pane_tree.end(root); ++ children) {
      if(children->fixed) -- children_cnt;
    }
    double f = 1.0 / (double) children_cnt;

    for(children = _pane_tree.begin(root); children != _pane_tree.end(root); ++ children) {
      if(children->fixed)
        children->real.factor = 0.0;
      else
        children->real.factor = f;
      SetEqualPaneSize((HPANE)children.node);
    }
  }

  // set equal size (width for horizontal and height for vertical) for every line in the control severally.
  void SetEqualPaneSize() {
    tree<struct pane_t>::iterator root((tree_node_<struct pane_t>*)_pane_root);

    root->real.factor = 1.0;

    SetEqualPaneSize(_pane_root);
    /*int cnt = _pane_tree.number_of_siblings(_pane_tree.begin(root));

    for(tree<struct pane_t>::sibling_iterator slibing = _pane_tree.begin(root);
        slibing != _pane_tree.end(root); ++ slibing) {
      if(slibing->fixed) -- cnt;
    }

    double f = 1.0 / (double) cnt;
    for(tree<struct pane_t>::sibling_iterator slibing = _pane_tree.begin(root);
        slibing != _pane_tree.end(root); ++ slibing) {
      if (slibing->fixed)
        slibing->real.factor = 0.0;
      else
        slibing->real.factor = f;
      if(_pane_tree.number_of_children(slibing)) {
        SetEqualPaneSize((HPANE)slibing.node);
      }
    }*/
  }

  void RecalcSplitterBar(WindowDefer* dw, HPANE pane, CRect rc, bool redraw) {
    if(0 == pane)
      ATLASSERT(FALSE);

    tree<struct pane_t>::iterator root((tree_node_<struct pane_t>*)pane);
    root->real.rect_pane = rc;

    int32_t cnt = _pane_tree.number_of_children(root);
    if(0 == cnt) {   // no children, move the pane window.
      if (dw && IsWindow(root->view_wnd)) {
        dw->MoveWindow(root->view_wnd, rc, redraw);
      }
      return;
    }

    CRect rect = rc;
    CSize splt_size;
    splt_size = root->real.bar_size;
    if(splt_size == CSize(0, 0))
      splt_size = GetSplitterBarSize();

    //int cx, cy;
    int bar_size = 0;
    int t_size = 0;
    int min_size = 0;
    int pos = 0, last_pos = 0;;
    if(root->horz) {
      t_size = rect.Width();
      bar_size = splt_size.cx;
      min_size = _pane_min_size.cx;
      pos = rect.left;
      last_pos = rect.right;
    } else {
      t_size = rect.Height();
      bar_size = splt_size.cy;
      min_size = _pane_min_size.cy;
      pos = rect.top;
      last_pos = rect.bottom;
    }

    if(1) {
      int expand_size = 0, total_extra_size = 0;
      int size = 0;
      int total_size = max(0, t_size) - (cnt - 1) * bar_size;

      if(total_size < cnt * min_size)
        min_size = max(0, total_size / cnt);

      // calculate the real size except the fixed panes.
      for(tree<struct pane_t>::sibling_iterator slibing = _pane_tree.begin(root);
          slibing != _pane_tree.end(root); ++ slibing) {
        if(_pane_tree.next_sibling(slibing) == _pane_tree.end(root)) {
          size = last_pos - pos;
        } else {
          size = (int)((double) total_size * slibing->real.factor + 0.5);
        }

        if(slibing->fixed) {
          size = slibing->fixed_size;
          expand_size += size;
        } else if(size < min_size)
          expand_size += (min_size - size);
        else
          total_extra_size += (size - min_size);
        pos += (size + bar_size);
      }

      if(root->horz)
        pos = rc.left;
      else
        pos = rc.top;

      for(tree<struct pane_t>::sibling_iterator slibing = _pane_tree.begin(root);
          slibing != _pane_tree.end(root); ++ slibing) {
        if(_pane_tree.next_sibling(slibing) == _pane_tree.end(root)) {
          size = last_pos - pos;
        } else if(slibing->fixed) {
          size = slibing->fixed_size;
        } else {
          size = (int)((double) total_size * slibing->real.factor + 0.5);
        }

        if(_pane_tree.next_sibling(slibing) != _pane_tree.end(root))  // except last column.
          if(size > min_size)
            size -= (int)((float)expand_size * ((float)(size - min_size) /
                                                (float)total_extra_size) + 0.5f);
        size = max(size, min_size);

        if(root->horz) {
          rect.left = pos;
          rect.right = pos + size;
        } else {
          rect.top = pos;
          rect.bottom = pos + size;
        }

        RecalcSplitterBar(dw, (HPANE)slibing.node, rect, redraw);
        pos += (size + bar_size);

        if(_pane_tree.next_sibling(slibing) != _pane_tree.end(root)) {
          if(root->horz) {
            slibing->real.rect_splitter.SetRect(rect.right, rect.top, rect.right + bar_size,
                                                rect.bottom);
            /*if (dw && IsWindow(slibing->view_wnd)) {
              dw->MoveShowWindow(slibing->view_wnd, slibing->real.rect_splitter);
            }*/
          } else {
            slibing->real.rect_splitter.SetRect(rect.left, rect.bottom, rect.right,
                                                rect.bottom + bar_size);
          }
        } else {
          slibing->real.rect_splitter.SetRectEmpty();
        }
      }
    }
  }

  void RecalcSplitterBar(CRect rect_pane, bool redraw) {
    if(0 == _pane_root) return;
    WindowDefer dw;
    RecalcSplitterBar(&dw, _pane_root, rect_pane, redraw);
  }

protected:
  void DrawSplitter(CDCHandle dc, HPANE parent) {
    T* pT = static_cast<T*>(this);
    tree<struct pane_t>::iterator root((tree_node_<struct pane_t>*)parent);
    for(tree<struct pane_t>::sibling_iterator slibing = _pane_tree.begin(root);
        slibing != _pane_tree.end(root); ++ slibing) {
      pT->DrawSplitterBar(dc, (HPANE)slibing.node, slibing->horz, slibing->real.rect_splitter);
      if(_pane_tree.number_of_children(slibing))
        DrawSplitter(dc, (HPANE)slibing.node);
    }
  }

  void DrawSplitter(CDCHandle dc) {
    if(0 == _pane_root) return;
    /*dc.FillSolidRect(0, 0, 100, 100, RGB(255, 0, 0));
    return;*/
    T* pT = static_cast<T*>(this);
    tree<struct pane_t>::iterator root((tree_node_<struct pane_t>*)_pane_root);
    //int cnt = _pane_tree.number_of_siblings(_pane_tree.begin(root));
    for(tree<struct pane_t>::sibling_iterator slibing = _pane_tree.begin(root);
        slibing != _pane_tree.end(root); ++ slibing) {
      pT->DrawSplitterBar(dc, (HPANE)slibing.node, slibing->horz, slibing->real.rect_splitter);
      if(_pane_tree.number_of_children(slibing)) {
        DrawSplitter(dc, (HPANE)slibing.node);
      }
    }
  }

  void DrawSplitterBar(CDCHandle dc, HPANE /*pane*/, bool /*horizontal*/, CRect rect_bar) {
    //ATLASSERT(FALSE);
    dc.FillSolidRect(rect_bar, ::GetSysColor(COLOR_BTNFACE));
  }

  void DrawSplitterDragRect(CDCHandle dc, bool /*horz*/, CRect rect, CRect rect_old) {
    ATLTRACE(_T("new(%d, %d, %d, %d), old(%d, %d, %d, %d)\n"),
             rect.left, rect.top, rect.right, rect.bottom,
             rect_old.left, rect_old.top, rect_old.right, rect_old.bottom);
    if(rect_old.IsRectNull()) {   // first draw
      DrawDragBarRect(dc, rect);
    } else if(rect.IsRectNull()) {   // lase draw
      DrawDragBarRect(dc, rect_old);
    } else {
      DrawDragBarRect(dc, rect_old);
      DrawDragBarRect(dc, rect);
    }
  }

  void DrawDragRect(CRect rect, bool horz) {
    static CRect rect_old(0, 0, 0, 0);

    T* pT = static_cast<T*>(this);
    CDCHandle dc = pT->GetDCEx(0, DCX_CACHE | DCX_LOCKWINDOWUPDATE);
    DrawSplitterDragRect(dc, horz, rect, rect_old);
    pT->ReleaseDC(dc.Detach());

    if(rect.IsRectNull()) {
      rect_old.SetRectEmpty();
    } else {
      rect_old = rect;
    }
  }

  void DrawDragBarRect(CDCHandle dc, const CRect rect) {
    CBrush br;
    br.CreateSolidBrush(RGB(34, 34, 34));
    HBRUSH old_brush = dc.SelectBrush(br);
    dc.PatBlt(rect.left, rect.top, rect.Width(), rect.Height(), PATINVERT);
    dc.SelectBrush(old_brush);
  }

protected:
  struct pane_t {
    uint64_t data;
    bool active_splitter;
    bool horz;
    bool dynamic_bar;
    bool fixed;
    uint32_t fixed_size;
    HWND view_wnd;
    struct state_t {
      CRect rect_pane, rect_splitter;
      CRect rect_min;
      CSize bar_size;
      double factor;
    } real;
    TCHAR name[PANE_NAME_LENGTH];
  };

  tree<struct pane_t> _pane_tree;
  std::set<HPANE> _pane_sets;

  CSize _splitter_size;
  CSize _pane_min_size;
  HPANE _pane_root;
  HPANE _cur_hit_pane, _drag_pane;
  CPoint _pt_split_drag_start;
};

#endif // __WND_SPLITTER_BAR_H__
