#ifndef __BAR_CONTROL_H__
#define __BAR_CONTROL_H__

#include <stdint.h>
#include <assert.h>
#include "animation.h"
#include "img_helpers.h"

class BarControl : public AnimationWidget {
public:
  enum { FADE_TIMEOUT = 200 };
  enum { NO_FADE = 0 };

  typedef struct tagBUTTON {
    uint32_t cmd_id;            // WM_COMMAND identifier
    int32_t icon_id;              // button image id
    int32_t bak_img_id;        // background image id.
    uint32_t cur_state;         // Current ODS_xxx state
    uint32_t old_state;         // Previous ODS_xxx state
    uint32_t start_tick;         // Time when fade started
    uint32_t timeout_tick;     // Time when fade should end
    uint32_t flags;               // button flags.
    RECT btn_rect;               // Current rect coordinates
    SIZE btn_size;
    TCHAR btn_text[100];     // Button Text
  } BUTTON;
  typedef CSimpleValArray<BUTTON> ButtonArray;

public:
  BarControl() {}

  BEGIN_MSG_MAP(BarControl)
    MESSAGE_HANDLER(WM_LBUTTONDOWN, OnLButtonDown)
    MESSAGE_HANDLER(WM_LBUTTONUP, OnLButtonUp)
    MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
    MESSAGE_HANDLER(WM_MOUSELEAVE, OnMouseLeave)
  END_MSG_MAP()

  virtual LRESULT OnLButtonDown(UINT , WPARAM, LPARAM lParam, BOOL&bHandled) {
    CPoint pt(lParam);
    if(!_bar_rect.PtInRect(pt)) {
      bHandled = FALSE;
      return 0;
    }

    //LOG_TRACE(DBG, "caption left down");
    int bar_idx = -1;
    CRect rect;
    if(FindItemFromPos(pt, bar_idx)) {
      rect = SetItemState(_buttons_array, bar_idx, ODS_CHECKED,
                          ODS_CHECKED | ODS_HOTLIGHT, NO_FADE);
      _clicked_idx = bar_idx;
      if(!rect.IsRectEmpty())
        InvalidateRect(_parent_wnd, rect, FALSE);
      return 0;
    }

    _clicked_idx = bar_idx;
    if(_clicked_idx < 0)
      bHandled = FALSE;

    return 0;
  }

  virtual LRESULT OnLButtonUp(UINT, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled) {
    CPoint pt(lParam);
    if(!_bar_rect.PtInRect(pt)) {
      bHandled = FALSE;
      return 0;
    }

    int btn_idx;
    if(!FindItemFromPos(pt, btn_idx)) {
      bHandled = FALSE;
      return 0;
    }

    _clicked_idx = -1;
    CRect rect = SetItemState(_buttons_array, btn_idx, ODS_HOTLIGHT,
                              ODS_CHECKED | ODS_HOTLIGHT, NO_FADE);

    if(!rect.IsRectEmpty())
      InvalidateRect(_parent_wnd, rect, FALSE);
    SendBtnMessage(btn_idx);
    return 0;
  }

  virtual LRESULT OnMouseMove(UINT, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled) {
    CPoint pt(lParam);
    if(!_bar_rect.PtInRect(pt)) {
      bHandled = FALSE;
      return 0;
    }

    int bar_pos;
    CRect rect;
    rect.SetRectEmpty();

    FindItemFromPos(pt, bar_pos);
    if(_clicked_idx >= 0 && _clicked_idx == bar_pos) {
      rect = SetItemState(_buttons_array, bar_pos, ODS_HOTLIGHT,
                          ODS_HOTLIGHT , NO_FADE);
    } else {
      if(_hover_idx != bar_pos /*&& bar_pos >= 0*/)
        rect = SetItemState(_buttons_array, bar_pos, ODS_HOTLIGHT,
                            ODS_HOTLIGHT | ODS_CHECKED , FADE_TIMEOUT);
      _clicked_idx = -1;
    }
    _hover_idx = bar_pos;
    if(!rect.IsRectEmpty())
      InvalidateRect(_parent_wnd, rect, FALSE);
    bHandled = FALSE;

    return 0;
  }

  virtual LRESULT OnMouseLeave(UINT, WPARAM /*wParam*/, LPARAM /*lparam*/, BOOL& bHandled) {
    CRect rect, update_rect;
    rect.SetRectEmpty();
    update_rect.SetRectEmpty();

    //FindItemFromPos(pt, bar_pos);
    if(_clicked_idx >= 0) {
      rect = SetItemState(_buttons_array, _clicked_idx, 0,
                          ODS_HOTLIGHT | ODS_CHECKED , NO_FADE);
      _clicked_idx = -1;
      update_rect.UnionRect(update_rect, rect);
    }

    if(_hover_idx >= 0) {
      rect = SetItemState(_buttons_array, _hover_idx, 0,
                          ODS_HOTLIGHT | ODS_CHECKED , NO_FADE);
      _hover_idx = -1;
      update_rect.UnionRect(update_rect, rect);
    }

    if(!rect.IsRectEmpty())
      InvalidateRect(_parent_wnd, rect, FALSE);
    bHandled = FALSE;
    return 0;
  }

public:
  virtual bool CalculateUpdate(HWND& hwnd, RECT &rect) {
    CRect rc;
    hwnd = _parent_wnd;
    bool still_animating = false;
    DWORD tick = ::GetTickCount();
    for(int32_t i = 0; i < _buttons_array.GetSize(); ++ i) {
      BUTTON &btn = _buttons_array[i];
      if(0 == btn.timeout_tick)
        continue;
      rc.UnionRect(rc, &btn.btn_rect);

      if(btn.timeout_tick >= tick)
        still_animating = true;
      else {
        still_animating = true;
        btn.start_tick = btn.timeout_tick = 0;
      }
    }
    rect = rc;
    return still_animating;
  }

  virtual void UpdateLayout(CRect rect) = 0;
  virtual void DoPaint(CDCHandle dc) {
    CDC dc_bmp;
    dc_bmp.CreateCompatibleDC(dc);

    for(uint32_t i = 0; i < GetButtonCount(); i++) {
      const BUTTON& btn = _buttons_array[i];
      if(btn.bak_img_id > 0) {
        CBitmapHandle bkg = GetImage(btn.bak_img_id);
        if(bkg.m_hBitmap) {
          BITMAP bm_info;
          HBITMAP org_bmp = dc_bmp.SelectBitmap(bkg);
          bkg.GetBitmap(bm_info);
          PaintButton(dc, btn, dc_bmp.m_hDC, bm_info, CRect(0, 0, 0, 0));
          dc_bmp.SelectBitmap(org_bmp);
        }
      }
      if(btn.icon_id > 0) {   // button has icon to display.
        CBitmapHandle icon = GetImage(btn.icon_id);
        if(icon.m_hBitmap) {
          BITMAP icon_info;
          HBITMAP org_bmp = dc_bmp.SelectBitmap(icon);
          icon.GetBitmap(icon_info);
          PaintButton(dc, btn, dc_bmp.m_hDC, icon_info, CRect(0, 0, 0, 0), IMGDRAWMODE_CENTER);
          dc_bmp.SelectBitmap(org_bmp);
        }
      }
    }
  }
  virtual CBitmapHandle GetImage(int32_t /*img_id*/) { return 0; }

  uint32_t GetButtonCount() const {
    return _buttons_array.GetSize();
  }

  void AddButton(uint32_t cmd_id, int32_t btn_bk_id, int32_t img_id = -1,
                 const TCHAR* caption = 0, SIZE size = CSize(0, 0)) {
    BUTTON btn = {0};
    POINT ptStart;
    ptStart.x = ptStart.y = 0;
    btn.btn_size = size;
    btn.cmd_id = cmd_id;
    btn.icon_id = img_id;
    btn.bak_img_id = btn_bk_id;
    if(caption && _tcslen(caption))
      _tcscpy(btn.btn_text, caption);
    _buttons_array.Add(btn);
  }

  int32_t SetButtonIcon(uint32_t btn_idx, int32_t icon_id, bool redraw = true) {
    if(btn_idx >= GetButtonCount()) return -1;
    BUTTON &btn = _buttons_array[btn_idx];
    int32_t old_id = btn.icon_id;
    btn.icon_id = icon_id;
    if(old_id != icon_id && redraw) {
      InvalidateRect(_parent_wnd, &btn.btn_rect, FALSE);
    }
    return 0;
  }

protected:
  void PaintButton(CDCHandle dc, const BUTTON& btn, CDCHandle dc_src, BITMAP src_img_info,
                   CRect margins, uint32_t mode = 0) {
    // First, paint the button (or 2 buttons when fading between states)...
    if(btn.start_tick == 0 || (btn.timeout_tick - btn.start_tick == 0)) {
      CRect rcSrc = CalcButtonBitmapRect(src_img_info, btn.cur_state);
      AlphaBlendStretch(dc, btn.btn_rect, dc_src, rcSrc, 255, margins, mode);
    } else {
      CRect img_src1 = CalcButtonBitmapRect(src_img_info, btn.cur_state);
      CRect img_src2 = CalcButtonBitmapRect(src_img_info, btn.old_state);
      uint32_t cur_tick = ::GetTickCount();
      if(cur_tick > btn.timeout_tick) cur_tick = btn.timeout_tick;
      uint32_t dwFade = ((cur_tick - btn.start_tick) * 255) / (btn.timeout_tick -
                        btn.start_tick);
      AlphaBlendStretch(dc, btn.btn_rect, dc_src, img_src2, (uint8_t)(255 - dwFade), margins,
                        mode);
      AlphaBlendStretch(dc, btn.btn_rect, dc_src, img_src1, (uint8_t)(dwFade), margins, mode);
    }
    // Paint text
    if(0 == _tcslen(btn.btn_text))
      return;
    dc.SetBkMode(TRANSPARENT);
    dc.SetTextColor(RGB(0, 0, 0));
    RECT rcText = btn.btn_rect;
    dc.DrawText(btn.btn_text, _tcslen(btn.btn_text), &rcText,
                DT_CENTER | DT_VCENTER | DT_SINGLELINE);
  }
  virtual CRect CalcButtonBitmapRect(BITMAP /*BmpInfo*/, UINT /*uState*/)  const = 0;

  bool FindItemFromPos(POINT pt, int& btn_idx) const {
    for(btn_idx = 0; btn_idx < (int)GetButtonCount(); ++ btn_idx) {
      if(::PtInRect(&_buttons_array[btn_idx].btn_rect, pt))
        return true;
    }
    btn_idx = -1;
    return false;
  }

  uint32_t ResolveButtonState(uint32_t btn_state) {
    if((btn_state & ODS_CHECKED) != 0) return ODS_CHECKED;
    if((btn_state & ODS_DISABLED) != 0) return ODS_DISABLED;
    if((btn_state & ODS_HOTLIGHT) != 0) return ODS_HOTLIGHT;
    return 0;
  }

  CRect SetItemState(ButtonArray& buttons, int pos, uint32_t add_state,
                     uint32_t remove_state, uint32_t timeout) {
    CRect rect_refresh;
    rect_refresh.SetRectEmpty();

    // Set the state on the chosen index; clear the state from all other buttons...
    for(int i = 0; i < buttons.GetSize(); i++) {
      BUTTON& btn = buttons[i];
      if(i != pos && (btn.cur_state & remove_state) != 0) {
        // If currently shown button state is affected, we start a new animation
        if((ResolveButtonState(btn.cur_state) & remove_state) != 0) {
          btn.start_tick = ::GetTickCount();
          btn.timeout_tick = btn.start_tick + timeout;
          //SetTimer(_parent_wnd, TIMERID_FADE, 25, 0);
          if(btn.timeout_tick > btn.start_tick)
            _host->StartAnimation(this);
        }
        btn.old_state = btn.cur_state;
        btn.cur_state &= ~remove_state;
        if(btn.timeout_tick > btn.start_tick)
          rect_refresh.UnionRect(rect_refresh, &btn.btn_rect);
      }

      if(i == pos /*&& (Btn.uCurState & uAddState) == 0*/) {
        // Switch to new state immediately...
        if((ResolveButtonState(btn.cur_state) & add_state) != 0) {
          btn.start_tick = btn.timeout_tick = ::GetTickCount();
        }
        btn.old_state = btn.cur_state;
        btn.cur_state &= ~remove_state;
        btn.cur_state |= add_state;
        rect_refresh.UnionRect(rect_refresh, &btn.btn_rect);
      }
    }

    return rect_refresh;
  }

  void SendBtnMessage(uint32_t btn_idx) {
    assert(btn_idx >= 0 && btn_idx < GetButtonCount());
    const BUTTON& btn = _buttons_array[btn_idx];
    PostMessage(_parent_wnd, WM_COMMAND, MAKEWPARAM(btn.cmd_id, 0), 0);
  }
protected:
  HWND _parent_wnd;
  ButtonArray _buttons_array;
  int32_t _clicked_idx, _hover_idx;
  CRect _bar_rect;
};


#endif // __BAR_CONTROL_H__
