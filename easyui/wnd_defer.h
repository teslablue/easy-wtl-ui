#ifndef __WND_DEFER_H__
#define __WND_DEFER_H__

#include <list>

class WindowDefer {
public:
  WindowDefer() {}
  virtual ~WindowDefer() {
    Process();
  }

  //
  bool MoveWindow(HWND wnd, RECT const *rc, bool redraw) { // move window.
    _ASSERTE(::IsWindow(wnd) != 0);
    //
    return Add(wnd, rc, redraw, false);
  }

  //
  bool MoveShowWindow(HWND wnd, RECT const *rc, bool redraw) { // move and show window.
    _ASSERTE(::IsWindow(wnd) != 0);
    //
    return Add(wnd, rc, redraw, true);
  }

  int Process() {
    int count = (int)windows.size();
    //
    if(count > 0) {
      HDWP dwp = ::BeginDeferWindowPos(count);
      //
      if(dwp != NULL) {
        for(i_windows i = windows.begin(), e = windows.end(); i != e; ++i) {
          Window &w = *i;
          bool needShow = (w.show == true && ::IsWindowVisible(w.wnd) == 0);
          //
          if(HasWindowPos(w.wnd, &w.rc) == false || needShow == true)
            ::DeferWindowPos(dwp, w.wnd, 0, w.rc.left, w.rc.top, w.rc.Width(), w.rc.Height(),
                             SWP_NOZORDER | SWP_NOACTIVATE | (needShow == true ? SWP_SHOWWINDOW : 0) |
                             (w.redraw == true ? 0 : SWP_NOREDRAW));
        }
        //
        ::EndDeferWindowPos(dwp);
      } else
        for(i_windows i = windows.begin(), e = windows.end(); i != e; ++i) {
          Window &w = *i;
          bool needShow = (w.show == true && ::IsWindowVisible(w.wnd) == 0);
          //
          if(HasWindowPos(w.wnd, &w.rc) == false || needShow == true)
            ::SetWindowPos(w.wnd, 0, w.rc.left, w.rc.top, w.rc.Width(), w.rc.Height(),
                           SWP_NOZORDER | SWP_NOACTIVATE | (needShow == true ? SWP_SHOWWINDOW : 0) |
                           (w.redraw == true ? 0 : SWP_NOREDRAW));
        }
      //
      windows.clear();
    }
    return count;
  }

  /////////////////////////////////////////////////////////////////////////////
private:
  /////////////////////////////////////////////////////////////////////////////
  //
  struct Window {
    HWND wnd;
    CRect rc;
    bool redraw, show;
  };
  //
  std::list<Window> windows;
  typedef std::list<Window>::iterator i_windows;

  /////////////////////////////////////////////////////////////////////////////
  //
  bool Add(HWND wnd, RECT const *rc, bool redraw, bool show) {
    try {
      windows.push_back(Window());
      Window &w = windows.back();
      w.wnd = wnd;
      w.rc = rc;
      w.redraw = redraw;
      w.show = show;
    } catch(std::bad_alloc &) {
      return false;
    }
    return true;
  }
  //
  bool HasWindowPos(HWND wnd,
                    RECT const *rc) const { // return 'true' if window has 'rc' position.
    CRect rcNow;
    ::GetWindowRect(wnd, &rcNow);
    ::MapWindowPoints(HWND_DESKTOP, ::GetParent(wnd), (POINT *)&rcNow,
                      2); // screen to parent's client.
    return (rcNow == *rc) != 0;
  }
};

#endif // __WND_DEFER_H__
