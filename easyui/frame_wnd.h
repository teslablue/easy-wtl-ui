#ifndef __BASIC_FRAME_H__
#define __BASIC_FRAME_H__

#include <stdint.h>
#include <atlmisc.h>

template <class T>
class BasicFrame {
public:
  BasicFrame() : _mouse_tracking(false), _min_track_size(0), _max_track_size(0) {}

  BEGIN_MSG_MAP(BasicFrame)
    MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)
    MESSAGE_HANDLER(WM_PAINT, OnPaint)
    MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
    MESSAGE_HANDLER(WM_MOUSELEAVE, OnMouseLeave)
    MESSAGE_HANDLER(WM_NCHITTEST, OnNcHitTest)     
    MESSAGE_HANDLER(WM_SIZE, OnSize)
    MESSAGE_HANDLER(WM_GETMINMAXINFO, OnGetMinMaxInfo)
  END_MSG_MAP()

  LRESULT OnEraseBackground(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/,
    BOOL& /*bHandled*/) {
      return 1;
  }

  LRESULT OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled) {
    T* pT = static_cast<T*>(this);
    ATLASSERT(::IsWindow(pT->m_hWnd));
    if (_wnd_conner.cx || _wnd_conner.cy) {
      CRect rect;
      pT->GetClientRect(rect);
      HRGN rgn = CreateRoundRectRgn(rect.left /*+ winStats.frm_clp_wdth*/,
        rect.top /*+ winStats.frm_clp_wdth*/
        , rect.right/*- winStats.frm_clp_wdth*/,
        rect.bottom/* - winStats.frm_clp_wdth*/, _wnd_conner.cx, _wnd_conner.cy);
      pT->SetWindowRgn(rgn);
      DeleteObject(rgn);
    }    

    bHandled = FALSE;
    return 0;
  }

  LRESULT OnGetMinMaxInfo(UINT, WPARAM, LPARAM lParam, BOOL& bHandled) {
    LPMINMAXINFO lpMMI = (LPMINMAXINFO) lParam;
    if (_min_track_size.x)
      lpMMI->ptMinTrackSize.x = _min_track_size.x;
    if (_min_track_size.y)
      lpMMI->ptMinTrackSize.y = _min_track_size.y;

    if (_max_track_size.y)
      lpMMI->ptMaxSize.y = _max_track_size.y;
    if (_max_track_size.x)
      lpMMI->ptMaxSize.x = _max_track_size.x;

    bHandled = FALSE;
    return 0;
  }

  LRESULT OnPaint(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/) {
    T* pT = static_cast<T*>(this);
    ATLASSERT(::IsWindow(pT->m_hWnd));
    
    if(wParam != NULL) {
      RECT rect = { 0 };
      pT->GetClientRect(&rect);
      CMemoryDC dcMem((HDC)wParam, rect);
      pT->DoEraseBkgnd(dcMem.m_hDC);
      pT->DoPaint(dcMem.m_hDC);
    } else {
      CPaintDC dc(pT->m_hWnd);
      CMemoryDC dcMem(dc.m_hDC, dc.m_ps.rcPaint);
      pT->DoEraseBkgnd(dcMem.m_hDC);
      pT->DoPaint(dcMem.m_hDC);
    }
    return 0;
  }

  LRESULT OnMouseMove(UINT, WPARAM, LPARAM, BOOL& bHandled) {
    bHandled = FALSE;
    if (!_mouse_tracking) _mouse_tracking = _StartTrackMouseLeave();
    return 0;
  }

  LRESULT OnMouseLeave(UINT, WPARAM, LPARAM, BOOL& bHandled) {
    bHandled = FALSE;
    _mouse_tracking = false;
    return 0;
  }

  bool _StartTrackMouseLeave() {
    T* pT = static_cast<T*>(this);
    TRACKMOUSEEVENT tme = { sizeof(tme), TME_LEAVE, pT->m_hWnd };
    return _TrackMouseEvent(&tme) == TRUE;
  }

  LRESULT OnNcHitTest(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled) {
    T* pT = static_cast<T*>(this);
    CPoint cur_pt(lParam);
    CRect rect_wnd;
    pT->GetWindowRect(&rect_wnd);

    if (cur_pt.x <= rect_wnd.left + _border_area.left && cur_pt.y <= rect_wnd.top + _border_area.top)
      return HTTOPLEFT;
    else if (cur_pt.x >= rect_wnd.right - _border_area.right && cur_pt.y <= rect_wnd.top + _border_area.top)
      return HTTOPRIGHT;
    else if (cur_pt.x <= rect_wnd.left + _border_area.left && cur_pt.y >= rect_wnd.bottom - _border_area.bottom)
      return HTBOTTOMLEFT;
    else if (cur_pt.x >= rect_wnd.right - _border_area.right && cur_pt.y >= rect_wnd.bottom - _border_area.bottom)
      return HTBOTTOMRIGHT;
    else if (cur_pt.x <= rect_wnd.left + _border_area.left)
      return HTLEFT;
    else if (cur_pt.x >= rect_wnd.right - _border_area.right)
      return HTRIGHT;
    else if (cur_pt.y <= rect_wnd.top + _border_area.top)
      return HTTOP;
    /*else if ((uint32_t)cur_pt.y <= rect_wnd.top + _border_area.top + _title_height)
    return HTCAPTION;*/
    else if (cur_pt.y >= rect_wnd.bottom - _border_area.bottom)
      return HTBOTTOM;

    bHandled = FALSE;
    return HTCLIENT;
  }

  void DoEraseBkgnd(CDCHandle dc) {
    ATLASSERT(FALSE);
  }

  void DoPaint(CDCHandle dc) {
    ATLASSERT(FALSE);
  }

  void SetWindowBorder(int l, int t, int r, int b) {
    _border_area.SetRect(l, t, r, b);
  }

  void SetWndConner(int32_t cx, int32_t cy) {
    _wnd_conner.cx = cx;
    _wnd_conner.cy = cy;
  }

  void GetWorkArea(LPRECT work_area) {
    T* pT = static_cast<T*>(this);
    CRect rect;
    pT->GetClientRect(&rect);
    rect.DeflateRect(&_border_area);
    rect.top += _title_height;
    *work_area = rect;
  }

  void SetTitleHeight(uint32_t height) {
    _title_height = height;
  }

protected:
  bool _mouse_tracking;  // Is currently tracking mouse?
  CRect _border_area;
  CSize _wnd_conner;
  uint32_t _title_height;
  CPoint _min_track_size, _max_track_size;
};

#endif // __BASIC_FRAME_H__
